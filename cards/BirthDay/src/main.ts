import {Init} from './loader'
import {init as initparallax} from './parralax'
import { confetti } from './confetti';
// Инит должен инициализировать CSS, затем передать JS переменные
Init().then(params => {
    initparallax();
    confetti();
})