export function init(

) {
    var element = document.getElementById("background2");

    // On mouse move
    function mouseMove(e) {

        var x = e.clientX
        var y = e.clientY;

        var width = window.innerWidth;
        var height = window.innerHeight;


        var ydiff = 22;
        var xdiff = 22;

        var ty = (ydiff * 2 * (y / height)) - ydiff;
        var tx = (xdiff * 2 * (x / width)) - xdiff;
        element.style.backgroundPosition = (tx) + 'px ' + (ty) + 'px';
    }

    document.body.addEventListener("mousemove", mouseMove);
}
