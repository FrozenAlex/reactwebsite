export interface Card {
    title: string; // Заголовок открытки
    description?: string; // Описание открытки
    settings: Array<ICardCSSSetting | ICardJSSetting | ICardTextSetting | ICardMusicSetting >
}

interface ICardCSSSetting {
    id: string,
    name: string,
    type: 'css',
    selector: string,
    property: string,
    value: string
}
interface ICardJSSetting {
    id: string,
    name: string,
    type: 'js',
    editor: 'color' | 'text' | 'slider',
    value: string
}

interface ICardTextSetting {
    id: string,
    name: string,
    type: 'text',
    selector: string,
    value: string
}

export interface ICardMusicSetting {
    id: string,
    name: string,
    type: 'music',
    path: string, // Путь к музыке (Bitch lasagna)
    track: Partial<Music> // Трек их базы данных (Для копирования)
}

export class Music{
    id: number;
    title: string;
    artist: string;
    path:string;
    
    // Жанр трека
    genre: Genre;
}

export class Genre {
    id: number;
    title: string;
    description?: string;

    // Music
    music: Music[]
}