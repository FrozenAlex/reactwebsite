import { Card } from "./types";
import { downloadManifest } from "./options";

/**
 * Проверяет находится ли окно в симуляции
 */
export function isSimulation() {
    return (window.location != window.parent.location)
}

// Функция инициализации
export function Init() {
    return new Promise(async (resolve, reject) => {
        // Качаем информацио о картинке и настройки по умолчанию
        let info = await downloadManifest()

        // Проверяем мы в симуляции или нет
        if (isSimulation()) {
            // Первое сообщение
            let params = await waitForMessage();
            // console.log(params.settings)
            await applyText(params.settings)

            // Начать сразу так как мы не ожидаем ввода
            document.body.classList.add('start'); // Loaded
            
            let overlay = document.getElementById("overlay") // Get start button
            overlay.remove()

            resolve(params)
            
            // Запустим цикл для сообщений
            MessageLoop();

        } else {
            // Устанавливаем название
            let overlay = document.getElementById("overlay") // Get start button

            let name = document.querySelector(".overlay .title");
            let description = document.querySelector(".overlay .subtitle");
            name.innerHTML = info.title;
            description.innerHTML = info.description;

            // Применить настройки текста
            applyText(info.settings);

            // Запуск открытки
            overlay.addEventListener("click", () => {
                // overlay.removeEventListener("click",)
                document.body.classList.add('start'); // Loaded

                // @ts-ignore
                let music = info.settings.filter((setting) => setting.type === 'music');
                music.forEach((item: any) => {
                    try {
                        let element = new Audio(item.path);
                        element.play();
                    } catch (err) {
                        console.log(err)
                    }
                })
                
                // Удалить Overlay после использования
                setTimeout(()=>{
                    overlay.remove()
                }, 3000);

                // Вернуть обработку вызывающему скрипту
                resolve(info);
            }, { once: true })
        }
    })
}

/**
 * Ждет сообщения от сервера
 */
function waitForMessage(): Promise<Partial<Card>> {
    return new Promise((resolve, reject) => {
        window.onmessage = (e: MessageEvent) => {
            let ass: Partial<Card> = JSON.parse(e.data);
            // TODO: Очистить событие
            resolve(ass);
        }
    })
}

// Продолжим слушать сообщения
async function MessageLoop() {
    let params = await waitForMessage();
    await applyText(params.settings)
    MessageLoop()
}

async function applyText(settings) {
    settings.forEach((setting) => {
        if (setting.type === 'text') {
            let element = document.querySelector(setting.selector);
            element.innerHTML = setting.value
        }
    })
}

// При запуске
function Start(params) {
    return new Promise((resolve, reject) => {
        let overlay = document.getElementById("overlay") // Get start button

        let name = document.querySelector(".overlay > title");
        name.innerHTML = params.title;

        // Запуск открытки
        overlay.addEventListener("click", () => {
            // overlay.removeEventListener("click",)
            document.body.classList.add('start'); // Loaded

            // Если в симуляции то не играть музыку
            if (isSimulation()) {
                resolve(params);
            }
            else {
                // @ts-ignore
                let music = params.settings.filter((setting) => setting.type === 'music');
                music.forEach((item: any) => {
                    try {
                        let element = new Audio(item.value);
                        element.play();
                    } catch (err) {
                        console.log(err)
                    }
                })
            }
        }, { once: true })
    })

}