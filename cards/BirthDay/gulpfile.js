"use strict";

const fs = require('fs-extra');
const archiver = require('archiver');
// Load plugins
const autoprefixer = require("autoprefixer");
const browsersync = require("browser-sync").create();
const cssnano = require("cssnano");
const del = require("del");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const uglify = require("gulp-uglify");
const cached = require("gulp-cached");


const webpack = require("webpack");
const webpackconfig = require("./webpack.config.js");
const webpackstream = require("webpack-stream");

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./build/"
        },
        port: 3000
    });
    done();
}

function zip(done) {
    var output = fs.createWriteStream('build.zip');
    var archive = archiver('zip', {
        zlib: {
            level: 9
        },
        comment: 'Открытка для сайта'
    });

    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        done();
    });

    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            console.log(err.message);
        } else {
            reject(err);
            throw err;
        }
    });

    archive.on('error', function (err) {
        throw(err);
    });
    // pipe archive data to the file
    archive.pipe(output);

    archive.directory(__dirname + '/build/', '/');
    archive.finalize();
}


// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Clean assets
function clean() {
    return del(["./build/"]);
}

// Clean assets
function copy() {
    return gulp
        .src('./public/**/*.*')
        .pipe(cached('public'))
        .pipe(gulp.dest("./build/"));
}

// Optimize Images
function images() {
    return gulp
        .src("./src/img/**/*")
        .pipe(newer("./build/img"))
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [{
                    removeViewBox: false
                }]
            })
        )
        .pipe(gulp.dest("./build/img"));
}

// CSS task
function css() {
    return gulp
        .src("./src/scss/**/*.scss")
        .pipe(plumber())
        .pipe(sass({
            outputStyle: "expanded"
        }))
        // .pipe(gulp.dest("./build/css/"))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest("./build/css/"))
        .pipe(browsersync.stream());
}

// Transpile, concatenate and minify scripts
function scripts() {
    return (gulp
        .src('./src/main.ts')
        .pipe(plumber())
        // .pipe(webpackstream(webpackconfig), webpack)
        .pipe(webpackstream(webpackconfig), webpack)
        .pipe(uglify())
        .pipe(gulp.dest("./build/"))
        .pipe(browsersync.stream())
    );
}

// Watch files
function watchFiles() {
    gulp.watch("./src/scss/**/*", css);
    gulp.watch("./src/**/*.ts", gulp.series(scripts));
    gulp.watch("./src/img/**/*", images);
    gulp.watch("./public/**/*", gulp.series(copy, browserSyncReload));
}

// Tasks
gulp.task("images", images);
gulp.task("css", css);
gulp.task("js", gulp.series(scripts));
gulp.task("clean", clean);

// build
gulp.task(
    "build",
    gulp.series(clean, gulp.parallel(copy, css, images, "js"),  zip)
);

// watch
gulp.task("watch", gulp.parallel(watchFiles, browserSync));