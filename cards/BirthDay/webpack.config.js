/**
 * Файл конфигурации Webpack
 */
module.exports = {
    output: {
        filename: 'bundle.js'
    },
    stats: {
        warnings: false
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    module: {
        rules: [{
            test: /\.(tsx|ts)$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: ["@babel/preset-env"]
                }
            }, {
                loader: 'ts-loader',
                options: {
                    // transpileOnly: true
                }
            }],
            exclude: /node_modules/
        }]
    },
    // optimization: {
    //     usedExports: true
    // },
    mode: 'production'
}