export interface Card {
    title: string; // Заголовок открытки
    description?: string; // Описание открытки
    settings: Array<ICardCSSSetting | ICardJSSetting | ICardTextSetting>
}

export interface ICardCSSSetting {
    id: string,
    name: string,
    type: 'css',
    selector: string,
    property: string,
    value: string
}

interface ICardTextSetting {
    id: string,
    name: string,
    type: 'text',
    selector: string,
    value: string
}

export interface ICardJSSetting {
    id: string,
    name: string,
    type: 'js',
    varname: string,
    value: string
}
