// Снег

let flakes = [],
  canvas: HTMLCanvasElement = document.getElementById("canvas") as HTMLCanvasElement,
  ctx = canvas.getContext("2d"),
  flakeCount = 100,
  speed = 20,
  mX = -100,
  mY = -100

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
let time = new Date().getTime();

export function Init(options: any = {}) {
  // Переписать некоторые опции если есть
  if (options) {
    flakeCount = options.flakeCount || flakeCount;
  }

  for (var i = 0; i < flakeCount; i++) {
    var x = Math.floor(Math.random() * canvas.width),
      y = Math.floor(Math.random() * canvas.height),
      size = (Math.random() * 3) + 2,
      speed = (Math.random() * 1) + 0.5,
      opacity = (Math.random() * 0.5) + 0.3;

    flakes.push({
      speed: speed,
      velY: speed,
      velX: 0,
      x: x,
      y: y,
      size: size,
      stepSize: (Math.random()) / 30,
      step: 0,
      opacity: opacity
    });
  }
};

export function Start() {
  snow();
}

// Снеег :)
function snow() {

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  // Установить следующие координаты
  let deltaTime = new Date().getTime() - time; // Разница во времени
  time = new Date().getTime()
  deltaTime /= 12;

  // Отрисовка снежинок на Canvas
  for (var i = 0; i < flakeCount; i++) {
    var flake = flakes[i],
      x = mX,
      y = mY,
      minDist = 150,
      x2 = flake.x,
      y2 = flake.y;

    var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
      dx = x2 - x,
      dy = y2 - y;

    if (dist < minDist) {
      var force = minDist / (dist * dist),
        xcomp = (x - x2) / dist,
        ycomp = (y - y2) / dist,
        deltaV = force / 2;

      flake.velX -= deltaV * xcomp;
      flake.velY -= deltaV * ycomp;

    } else {
      flake.velX *= .98;
      if (flake.velY <= flake.speed) {
        flake.velY = flake.speed
      }
      flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
    }

    ctx.fillStyle = `rgba(255,255,255,${flake.opacity})`;

    // Передвижение снежинок
    flake.y += flake.velY * deltaTime;
    flake.x += flake.velX * deltaTime;

    if (flake.y >= canvas.height || flake.y <= 0) {
      reset(flake);
    }

    if (flake.x >= canvas.width || flake.x <= 0) {
      reset(flake);
    }

    ctx.beginPath();
    ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
    ctx.fill();
  }
  window.requestAnimationFrame(snow);
};

function reset(flake) {
  flake.x = Math.floor(Math.random() * canvas.width);
  flake.y = 0;
  flake.size = (Math.random() * 4) + 2;
  flake.speed = (Math.random() * 1) + 0.5;
  flake.velY = flake.speed;
  flake.velX = 0;
  flake.opacity = (Math.random() * 0.5) + 0.3;
}

canvas.addEventListener("mousemove", function (e: MouseEvent) {
  mX = e.clientX
  mY = e.clientY
});

// Меняет размер
window.addEventListener("resize", function () {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
})