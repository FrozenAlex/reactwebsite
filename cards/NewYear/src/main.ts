import * as snow from './snow'
import { Init, isSimulation } from './loader'
import { Card, ICardMusicSetting } from './types';

// Инит должен инициализировать CSS, затем передать JS переменные
Init().then((params: Card) => {
    console.log(params)

    snow.Init({ flakeCount: 40 })
    snow.Start()
})

