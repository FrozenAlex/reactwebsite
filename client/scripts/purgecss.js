const Purgecss = require('purgecss')
const fs = require('fs')
const path = require('path')

const outputDir = path.resolve(__dirname, '..' ,'src', 'css')

const settings = require('./../config/purgecss.config')

const purgecss = new Purgecss(settings)
const result = purgecss.purge()

result.forEach(out => {
    const filePath = out.file.split('/')
    console.log(filePath)
	fs.writeFileSync(`${outputDir}${path.sep}${filePath[filePath.length - 1]}`, out.css, 'utf-8')
})

console.log('done')