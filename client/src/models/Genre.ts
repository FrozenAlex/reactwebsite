import { Music } from "./Music";

/**
 * Жанр музыки
 */
export class Genre {
    id: number;
    title: string;
    description?: string;

    // Music
    music: Music[]
}
