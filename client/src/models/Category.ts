import { Card } from "./Card";

/**
 * Категория открыток
 */
export class Category{
    id: number;
    title: string;
    description?: string;

    // Открытки, в этой категории
    cards: Card[];
}
