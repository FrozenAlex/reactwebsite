
import { Genre } from "./Genre";

/**
 * Музыкальная композиция
 */
export class Music{
    id: number;
    title: string;
    artist: string;
    path:string;
    
    // Жанр трека
    genre: Genre;
}
