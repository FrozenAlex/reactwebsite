import { Card } from "./Card";

/**
 * Пользователь системы
 */

export class User{
    id: number;

    // Отображаемое имя пользователя
    name: string;

    // Имя пользователя (логин)
    username: string;

    // Почта
    email: string;

    // Хэшированый пароль пользователя
    password: string;

    // Админ я или нет?
    admin: boolean;
    
    // Открытки, созданные пользователем
    cards: Card[];
}
