import { Category } from './Category';
import { User } from './User';
import { Music } from './Music';


/**
 * Открытка
 */
export class Card {
    id: number;
    title: string; // Заголовок открытки

    description?: string; // Описание открытки
    url: string; // Короткая ссылка на открытку
    public: boolean; // Публичная ли карточка
    settings: Array<ICardTextSetting | ICardJSSetting | ICardCSSSetting | ICardMusicSetting> // Данные о открытке в формате JSON
    category: Category;  // Категория открытки
    user: User; // Пользователь - создатель открытки

    dateCreated: Date; // Дата создания
    dateUpdated: Date; // Дата обновления (Не должно случаться, ну а вдруг)
}


export interface ICardCSSSetting {
    id: string,
    name: string,
    type: 'css',
    selector: string,
    property: string,
    value: string
}
export interface ICardJSSetting {
    id: string,
    name: string,
    type: 'js',
    editor: 'color' | 'text' | 'slider',
    value: string
}

export interface ICardTextSetting {
    id: string,
    name: string,
    type: 'text',
    selector: string,
    value: string
}

export interface ICardMusicSetting {
    id: string,
    name: string,
    type: 'music',
    path: string, // Путь к музыке ()
    track: Partial<Music> // Трек их базы данных (Для копирования)
}