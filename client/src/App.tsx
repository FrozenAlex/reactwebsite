import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import './css/App.css';
import HomePage from "./components/pages/Home";

import AboutPage from './components/pages/About';
import AnimationsPage from './components/cards/Animations';
import LoginPage from './components/auth/LogIn';
import RegisterPage from './components/auth/Register';
import MusicPage from './components/music/Music';
import EditorPage from './components/cards/CardEditor';
import MusicUploader from './components/music/MusicUploader';
import CardUploader from './components/cards/CardUploader';
import SettingsPage from './components/user/SettingsPage';
import CategoriesPage from './components/pages/Categories';
import AdminPage from './components/admin/AdminPage';

interface IApplicationState {
  user?: {
    username?: string,
    token?: string,
    info?: string
  }
}

export default class App extends React.Component<any, IApplicationState> {
  constructor(props: any) {

    super(props);
    this.state = {} // Начальное состояние приложения
  }

  public render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route exact={true} path='/' component={HomePage} />
            <Route path='/animations/' component={AnimationsPage} />
            <Route path='/about/' component={AboutPage} />
            <Route path='/login/' component={LoginPage} />
            <Route path='/register/' component={RegisterPage} />
            <Route path='/music/'  component={MusicPage} />
            <Route path='/upload/' component={MusicUploader} />
            <Route path='/cardup/' component={CardUploader} />
            <Route path='/editor/:id' component={EditorPage} />
            <Route path='/settings/' component={SettingsPage} />
            <Route path='/categories/' component={CategoriesPage} />
            <Route path='/admin/' component={AdminPage} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}
