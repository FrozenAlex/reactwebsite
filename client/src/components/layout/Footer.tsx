import * as React from 'react';
import svg from '../../images/footer.svg';

export default class Footer extends React.Component {
    public state = { showMenu: false }


    constructor(props: any) {
        super(props);
    }
    public render() {
        return (
            <div>
                {/* <div style={footerStyle} /> */}
                <img src={svg} style={{ marginBottom: 0, width: "100%", verticalAlign:"bottom" }} />
                <footer className="footer has-text-white" style={{ marginTop: 0, width: "100%" }}>
                    <div className="container">
                        <div className="level">
                            <div className="level-left">
                                <div className="has-text-centered">
                                    Кристина Емелина &copy; {new Date().getFullYear()}
                                </div>
                            </div>
                            <div className="level-right">
                                <a href="https://bulma.io" className="has-text-centered">
                                    <img src={require('../../images/bulma.png')} alt="Made with Bulma" width="128" height="24" />
                                </a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }

    public toggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
}
