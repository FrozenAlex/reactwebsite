import * as React from 'react';

export default class SplashScreen extends React.Component {
    public state = { showMenu: false }

    
    constructor(props: any) {
        super(props);
    }
    public render() {
        return (
            <div className="splash" id="splash">
                <h1>Открытки</h1>
                <div id="splashStatus">{{ status }}</div>
            </div>
        );
    }

    public toggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
}


