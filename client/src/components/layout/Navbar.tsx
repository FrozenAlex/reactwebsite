/*
    Компонент навигации
*/
import throttle from 'lodash/throttle';

import * as React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
// import { User } from 'src/models/User';
import { logoutUser } from 'src/redux/actions/authActions';
import OnClickOut from 'react-onclickoutside';

interface INavbarProps {
    fixed?: boolean,
    user?: any,
    logout: any
}

let currentScrollPos = 0;
let prevScrollpos = 0;

class Navbar extends React.Component<INavbarProps, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            isHidden: false,
            showMenu: false,
            leftMenu: [
                { name: 'Анимации', to: '/animations' },
                { name: 'Категории', to: '/categories' },
                { name: 'Музыка', to: '/music' },
                { name: 'О нас', to: '/about' }
            ]
        }

        prevScrollpos = window.pageYOffset;
        window.onscroll = this.handleScroll();
    }

    handleClickOutside = (evt: any) => {
        // ..handling code goes here...
        this.setState({ showMenu: false });
    };

    public handleScroll() {
        return throttle(() => {
            currentScrollPos = window.pageYOffset;

            if ((prevScrollpos > currentScrollPos) || (currentScrollPos < 51)) {
                this.setState({
                    isHidden: false
                });
            } else {
                this.setState({
                    isHidden: true,
                    showMenu: false
                });
            }
            prevScrollpos = currentScrollPos;
        }, 150)
    }


    public componentDidMount = () => {
        if (this.props.fixed) {
            document.body.classList.add("has-navbar-fixed-top")
        } else {
            document.body.classList.remove("has-navbar-fixed-top")
        }
    }

    public componentWillUnmount = () => {
        if (this.props.fixed) { document.body.classList.remove("has-navbar-fixed-top") }
    }


    public render() {
        // Fixed or not

        return (
            <nav className={`${(this.state.isHidden) ? 'is-up' : ''} is-transparent navbar has-shadow ${(this.props.fixed) ? 'is-fixed-top' : ''}`}>
                <div className="container">
                    <div className="navbar-brand">
                        <div className="navbar-item">
                            <NavLink to="/"><i>Открытки</i></NavLink>
                        </div>
                        <a role="button" className={`navbar-burger is-transparent burger ${(this.state.showMenu) ? 'is-active' : ''}`} onClick={this.toggleMenu}>
                            <span aria-hidden="true" />
                            <span aria-hidden="true" />
                            <span aria-hidden="true" />
                        </a>
                    </div>

                    <div className={`navbar-menu is-transparent ${(this.state.showMenu) ? 'is-active' : ''}`}>
                        <div className="navbar-start">
                            {/* Создание левого меню */}

                        </div>
                        <div className="navbar-end">
                            {this.state.leftMenu.map((item: any) =>
                                (<NavLink
                                    to={item.to}
                                    exact={item.exact}
                                    key={item.name}
                                    activeClassName="is-active"
                                    className="navbar-item"
                                    onClick={this.toggleMenuOff}>
                                    {item.name}
                                </NavLink>))
                            }

                            {(this.props.user) ? (
                                this.userMenu(this.props.user)
                            ) : (
                                    <React.Fragment>
                                        <NavLink onClick={this.toggleMenuOff} activeClassName="is-active" to="/register" className="navbar-item">
                                            <button className="button is-pink is-rounded">Регистрация</button>
                                        </NavLink>
                                        <NavLink onClick={this.toggleMenuOff} activeClassName="is-active" to="/login" className="navbar-item">
                                            <button className="button is-pink is-rounded is-outlined">Войти</button>
                                        </NavLink>
                                    </React.Fragment>
                                )}
                        </div>
                    </div>
                </div>

            </nav >
        );
    }

    private handleLogout = () => {
        // Toggle menu off
        this.toggleMenuOff();

        this.props.logout()
    }

    private toggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
    private toggleMenuOff = () => {
        this.setState({ showMenu: false });
    }

    private userMenu(user: any) {
        return (
            <div className="navbar-item has-dropdown is-hoverable">

                <a className="navbar-link">
                    {user.name}
                </a>
                <div className="navbar-dropdown is-boxed">
                    <NavLink
                        className="navbar-item"
                        activeClassName="is-active"
                        onClick={this.toggleMenuOff}
                        to="/admin/">
                        <span>
                            <i className="fa fa-tachometer-alt" /> Админка
                        </span>
                    </NavLink>
                    <NavLink
                        className="navbar-item"
                        activeClassName="is-active"
                        onClick={this.toggleMenuOff}
                        to="/settings/">
                        <span>
                            <i className="fa fa-cog" /> Настройки
                        </span>
                    </NavLink>
                    <a
                        className="navbar-item"
                        onClick={this.handleLogout}>
                        <span>
                            <i className="fa fa-sign-out-alt" /> Выйти
                        </span>
                    </a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user.info
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        logout: () => { dispatch(logoutUser()) }
    }
}

// Хак... Так как redux блокирует события от router оборачиваем его :) 
// TypeScript конечно же ругается по этому игнорить
// @ts-ignore
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OnClickOut(Navbar)))