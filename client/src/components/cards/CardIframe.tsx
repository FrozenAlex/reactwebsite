import React, { Fragment } from 'react';
import { Card } from 'src/models/Card';
// import Axios from 'axios';

const FrameStyle: React.CSSProperties = {
    height: '100%',
    width: '100%'
}


interface ICardIFrameProps {
    card: Partial<Card>
}

interface ICardIFrameState {
    ref?: HTMLIFrameElement | null,
    content: string;
}

export default class CardIframe extends React.Component<ICardIFrameProps, ICardIFrameState>{
    frame: HTMLIFrameElement | null
    constructor(props: ICardIFrameProps) {
        super(props);

        if(typeof this.props.card.settings === "string") {
            this.props.card.settings = JSON.parse(this.props.card.settings)
        }

        this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this)
    }

    shouldComponentUpdate() {
        return false;
    }

    async componentDidMount() {
        if (this.frame && this.frame) {
            this.frame.onload = () => {
                if (this.frame && this.frame.contentWindow) {
                    this.frame.contentWindow.postMessage(JSON.stringify(this.props.card), '*');
                }
            }

            // if (this.frame && this.frame.contentWindow) {
            //     const cardUrl = `/card/${this.props.card.url}/`;
            //     const response = await Axios.get(`/card/${this.props.card.url}/index.html`);
            //     const result = response.data;

            //     this.frame.contentWindow.history.pushState(null,'', cardUrl)
            //     const doc = this.frame.contentWindow.document;
            //     doc.open();
            //     doc.write(result);
            //     doc.close()
            // }
        }




    }

    componentWillReceiveProps(nextProps: ICardIFrameProps) {
        if (this.frame && this.frame.contentWindow) {
            this.frame.contentWindow.postMessage(JSON.stringify(nextProps.card), '*');
        }
    }


    render() {
        return (
            <Fragment>
                <iframe src={`/card/${this.props.card.url}/`} frameBorder="0" style={FrameStyle} ref={(e) => this.frame = e} />
            </Fragment>
        )
    }

}