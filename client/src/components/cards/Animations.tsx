import * as React from 'react';
import { Card } from './../../models/Card';
import CardTile from './CardTile';
import Axios from 'axios';
import { ChangeEvent } from 'react';
import Navbar from '../layout/Navbar';

interface IState {
  cards: Array<Partial<Card>>,
  search: string
  isLoading: boolean
}
export default class AnimationsPage extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);

    // Начальное состояние
    this.state = {
      cards: [],
      search: '',
      isLoading: true
    }
  }

  public handleSearch = async (e: ChangeEvent<HTMLInputElement>) => {

    if (e.target.value !== this.state.search) {
      this.setState({
        search: e.target.value,
        isLoading: true
      })
    }
    const request = await Axios.get('/api/search', {
      params: {
        q: e.target.value
      }
    });

    const result: Array<Partial<Card>> = await request.data;

    result.map((card) => {
      if (typeof card.settings === 'string') {
        card.settings = JSON.parse(card.settings);
      }
      return card;
    })

    this.setState({
      cards: result,
      isLoading: false
    })
  }

  public async fetchData() {
    const request = await Axios.get('/api/card');

    const result: Array<Partial<Card>> = await request.data;

    result.map((card) => {
      if (typeof card.settings === 'string') {
        card.settings = JSON.parse(card.settings);
      }
      return card;
    })

    this.setState({
      cards: result,
      isLoading: false
    })
  }
  public componentDidMount() {
    this.fetchData();
  }

  public hadleClick(e: React.MouseEvent<HTMLDivElement>) {
    e.preventDefault();
  }
  public render() {
    return (
      <div>
        <Navbar fixed={false} />
        <div className="hero animations-hero">
          <div className="hero-body has-text-centered">
            <span className="title">Анимации</span>
            <div className="search-form">
              <div className="control">
                <input className="input"
                  type="text"
                  placeholder="Найти открытку"
                  onChange={this.handleSearch} />
              </div>
              <div className="control">
                <a className="search-button button is-primary">
                  <i className="fa fa-search"/>
                </a>
              </div>
            </div>

          </div>
        </div>
        <section>
          <div className="container">
            {
              (this.state.isLoading) ?
                (
                  <h1 className="title has-text-centered">Загрузка списка...</h1>
                ) : (
                  <div className="cards" style={{ display: 'flex', justifyContent: 'space-between', flexWrap: "wrap" }}>
                    {this.state.cards.map((card: Partial<Card>) => (

                      <CardTile key={card.id} card={card} onClick={this.hadleClick} />
                    )
                    )}
                  </div>
                )
            }
          </div>
        </section>
      </div>
    );
  }
}
