import { Component } from "react";
import React from 'react';
import { Music } from 'src/models/Music';

interface IMusicItemProps {
    onchange: any,
    item: Partial<Music>,
    isActive: boolean
}

class MusicItem extends Component<IMusicItemProps> {
    constructor(props: IMusicItemProps) {
        super(props)
    }

    handleClick = () => {
        this.props.onchange(this.props.item.id)
    }

    render() {
        return (
            <li className="item" key={this.props.item.id} onClick={this.handleClick}>
                <span><i className="fa fa-music" /> {this.props.item.title}</span>
            </li>
        )
    }
}

export default MusicItem;