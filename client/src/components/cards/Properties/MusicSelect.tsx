import React, { Component } from "react";
import { Music } from 'src/models/Music';
import OnClickOut from 'react-onclickoutside';
import MusicItem from './MusicItem';
import { ICardMusicSetting } from 'src/models/Card';

interface IMusicSelectState {
    open: boolean
    selectedMusic?: Partial<Music>
}

interface IMusicSelectProps {
    music: Array<Partial<Music>>
    onchange: any
    currentItem: Partial<Music>
    settings: ICardMusicSetting
}

/**
 * Компонент для выбора музыки
 */
class MusicSelect extends Component<IMusicSelectProps, IMusicSelectState>{

    constructor(props: IMusicSelectProps) {
        super(props)
        this.state = {
            open: false
        }
    }

    handleClickOutside() {
        this.setState({
            open: false
        })
    }

    public onItemChange() {
        this.props.onchange()
    }

    public toggleItem = (id: number) => {
        const track = this.props.music.find((music) => (music.id === id))

        if (track) {
            this.props.settings.track = track;

            this.props.settings.path = "";

            this.props.onchange(this.props.settings);
            this.setState({
                open: false
            })
        }

    }

    render() {
        return (
            <div className="music-select">
                <div
                    className="current"
                    onClick={this.toggleList}>
                    {(this.props.settings.track) ? (`${this.props.settings.track.title} ${this.props.settings.track.artist}`) : (`Выбор музыки`)}
                </div>
                {(this.state.open) ? <i className="fa fa-angle-up" /> : <i className="fa fa-angle-down" />}
                {<ul className={`list ${(this.state.open) ? "open" : ""}`}>
                    {this.props.music.map((item) => (
                        <MusicItem
                            key={item.id}
                            isActive={(item === this.props.currentItem)}
                            item={item}
                            onchange={this.toggleItem} />
                        // <li className="item" key={item.id} onClick={this.toggleItem} data-key={item.id} >
                        //     <span><i className="fa fa-music" /> {item.title}</span>
                        // </li>
                    ))}
                </ul>}
            </div>
        )
    }

    private toggleList = () => {
        // tslint:disable-next-line:no-console
        console.log("isopen" + this.state.open)
        this.setState({
            open: !this.state.open
        })
    }
}

export default OnClickOut(MusicSelect)