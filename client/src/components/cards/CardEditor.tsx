import * as React from 'react';
import CardIframe from './CardIframe';
import { Card, ICardMusicSetting } from 'src/models/Card';
import { RouteComponentProps, withRouter } from 'react-router';
import Axios from 'axios';
import { connect } from 'react-redux';
import { Music } from 'src/models/Music';
import MusicSelect from './Properties/MusicSelect';
import Navbar from '../layout/Navbar';

interface IEditorProps {
  card?: Partial<Card>
  iframe?: HTMLIFrameElement | null
}

interface IEditorState {
  card?: Partial<Card>
  iframe?: HTMLIFrameElement | null,
  music: Array<Partial<Music>>
}

class EditorPage extends React.Component<RouteComponentProps<{ id: string }> & IEditorProps, IEditorState> {
  constructor(props: RouteComponentProps<{ id: string }> & IEditorProps) {
    super(props);

    this.state = {
      card: undefined,
      iframe: null,
      music: []
    }
    this.updateSetting = this.updateSetting.bind(this)
  }

  public afterLoad(e: React.SyntheticEvent<HTMLIFrameElement>) {
    e.preventDefault();
  }

  // Update music change
  public onMusicChange = (setting: ICardMusicSetting) => {
    // 
    if (this.state.card && this.state.card.settings) {
      const newSettings = this.state.card.settings;

      const index = newSettings.findIndex((item) => ((item.type === "music") && (item.id === setting.id)));
      
      // tslint:disable-next-line:no-console
      console.log(newSettings)
      // tslint:disable-next-line:no-console
      console.log(setting);
      if (index >= 0) {
        newSettings[index] = setting;

        // tslint:disable-next-line:no-console
        console.log(newSettings)
        this.setState({
          card: {
            ...this.state.card,
            settings: newSettings
          }
        })
      }

    }
  }


  public updateSetting(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    e.preventDefault();
    const key = e.target.getAttribute('data-key');

    if (this.state.card && this.state.card.settings) {
      const newSettings = this.state.card.settings;
      if (key) {
        const itemIndex = this.state.card.settings.findIndex(setting => setting.id.toString() === key)
        if (itemIndex >= 0) {
          const item = newSettings[itemIndex];

          let shouldReload = false;

          // Применение параметров различных типов
          if (item.type === "text") {
            item.value = e.target.value;
          }

          if (item.type === "css") {
            item.value = e.target.value;
          }

          if (item.type === "js") {
            item.value = e.target.value;
            shouldReload = true;
          }

          // Нужна особая обработка этого параметра
          if (item.type === "music") {
            // tslint:disable-next-line:no-console
            console.log("asdasadd");

            (item as ICardMusicSetting).path = ""; // Убрать путь (для того чтобы скопировать новую)
            // tslint:disable-next-line:no-console
            console.log(e.target.value);
          }
          // newSettings[itemIndex] = item;
          this.setState({
            card: {
              ...this.state.card,
              settings: newSettings
            }
          })

          if (shouldReload) {
            return
          }
        }
      }
    }

  }

  public async componentDidMount() {
    // Сделать 2 запроса
    const [cardResponse, musicResponse] = await Promise.all([
      Axios.get(`/api/card/${this.props.match.params.id}`),
      Axios.get(`/api/music`)
    ])

    // Взять все данные
    const card: Partial<Card> = cardResponse.data;
    const music: Array<Partial<Card>> = musicResponse.data;

    this.setState({ card, music });
  }

  public render() {
    let options;
    if (this.state && this.state.card && this.state.card.settings) {
      options = this.state.card.settings.map(
        (param) => {

          // Текстовый параметр
          if (param.type === 'text') {
            return (
              <div className="field" key={param.id}>
                <label className="label">{param.name}</label>
                <div className="control">
                  <input
                    data-key={param.id}
                    className="input"
                    type="text"
                    value={param.value}
                    onChange={this.updateSetting} />
                </div>
              </div>
            )
          }

          // Музыкальный параметр
          if (param.type === "music") {
            return (
              <div className="field" key={param.id}>
                <label className="label">{param.name} Music</label>
                <div className="control">
                  <MusicSelect
                    key={param.id}
                    music={this.state.music}
                    settings={param}
                    currentItem={param.track}
                    onchange={this.onMusicChange}
                  />

                </div>
              </div>
            )
          }

          return (
            <div className="field" key={param.id}>
              <label className="label">{param.name}</label>
              <div className="control">
                <input
                  data-key={param.id}
                  className="input"
                  type="text"
                  placeholder="Text input"
                  onChange={this.updateSetting} />
              </div>
            </div>
          )
        }
      )
    }

    return (
      <div>
        <Navbar fixed={true} />
        <div className="">
          <div className="columns">
            <div className="column editor-card" style={{ height: 'calc(100vh - 3.4em)' }}>
              {
                (this.state.card) ? (<CardIframe card={this.state.card} />) : (
                  <div>Карточки нет!</div>
                )
              }
            </div>
            <div className="column is-one-third card-options" style={{
              padding: '1em'
            }}>
              <h3 className="title">Свойства открытки #{(this.state.card) ? this.state.card.url : ''}</h3>
              {options}
              <button className="button is-rounded is-primary" onClick={this.saveCard}><i className="fa fa-save" /></button>
              <button className="button is-rounded is-primary" onClick={this.forkCard}><i className="fa fa-copy" /></button>
              <button className="button is-rounded" onClick={this.deleteCard}><i className="fa fa-trash" /></button>
              <a className="button is-rounded"
                href={`/card/${(this.state.card) ? (this.state.card.url) : (null)}/`}
                target="__blank"><i className="fa fa-external-link-alt" /></a>
              <hr />

              <div className="level is-mobile">
                <div className="level-left">
                  <h3 className="subtitle">Поделиться</h3>
                </div>
                <div className="level-right">
                  <div className="share-buttons">
                    {socialButtons(this.state.card)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  private saveCard = async () => {
    const token = localStorage.getItem("token");
    if (this.state.card && token) {
      const payload = this.state.card;
      const response = await Axios.put(`/api/card/${this.state.card.id}`, payload,
        { headers: { Authorization: token } });

      // Если нет прав, то убрать
      // tslint:disable-next-line:no-console
      console.log(response.data);
      // this.setState({card:newCard});
    } else {
      alert("Войдите или зарегистрируйтесь для создания открыток")
    }

  }

  private deleteCard = async () => {
    const token = localStorage.getItem("token");
    if (this.state.card && token) {
      const response = await Axios.delete(`/api/card/${this.state.card.id}`,
        {
          headers: {
            Authorization: token
          }
        });
      // tslint:disable-next-line:no-console
      console.log(response.data);
      // this.setState({card:newCard});
      this.props.history.push('/animations');
      // this.componentDidMount();
    } else {
      alert("Войдите или зарегистрируйтесь для удаления открыток")
    }
  }

  private forkCard = async () => {
    const token = localStorage.getItem("token");
    if (this.state.card && token) {
      const payload = {
        card: this.state.card
      }
      const response = await Axios.put(`/api/card/${this.state.card.id}/fork`, {
        payload
      },
        {
          headers: {
            Authorization: token
          }
        });

      // tslint:disable-next-line:no-console
      console.log(response.data);
      // this.setState({card:newCard});
      this.props.history.push(`/editor/${response.data.url}/`);
      // let oldState = Object.assign({}, this.state.card);

      this.setState({
        card: response.data
      })
    } else {
      alert("Войдите или зарегистрируйтесь для создания копий открыток")
    }
  }
}


const socialButtons = (card: Partial<Card> | undefined) => {
  if (card) {
    const origin = window.location.origin;
    const cardUrl = `${origin}/card/${card.url}/`
    return (
      <React.Fragment>
        <a href={`https://vk.com/share.php?title=${card.title}&description=${card.description}&url=${cardUrl}`} className="vk" target="_blank"><i className="fab fa-vk" /></a>
        <a href={`http://www.facebook.com/sharer.php?url=${cardUrl}`} className="facebook" target="_blank"><i className="fab fa-facebook-f" /></a>
        <a href={`https://connect.ok.ru/offer?url=${cardUrl}&title=${card.title}`} className="odnoklassniki" target="_blank"><i className="fab fab fa-odnoklassniki" /></a>
      </React.Fragment>
    )
  } else {
    return (
      <React.Fragment >
        Открытка еще не загружена, подождите..
      </React.Fragment >
    )
  }
}


const mapStateToProps = (state: any) => {
  return {
    user: state.user.info
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {

  }
}




// @ts-ignore
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditorPage));
