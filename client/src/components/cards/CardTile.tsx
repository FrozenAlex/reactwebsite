import * as React from 'react';
import CardIframe from './CardIframe';
import { Card } from 'src/models/Card';
import { NavLink } from 'react-router-dom';
import { format } from 'date-fns'
import ru from 'date-fns/locale/ru'

interface ICardProps {
    key?: any
    card: Partial<Card>,
    onClick: any
}

export default class CardTile extends React.Component<ICardProps, any> {
    constructor(props: ICardProps) {
        super(props);
    }
    public render() {
        return (
            <div className="card" onClick={this.props.onClick}>
                <div className="card-image" style={{ height: '300px' }}>
                    <CardIframe card={this.props.card} />
                </div>
                <div className="card-content">
                    <NavLink to={`/editor/${this.props.card.url}` || ''}>
                        <span className="title">{this.props.card.title}</span>
                        <div className="content">{this.props.card.description}</div>
                    </NavLink>
                </div>
                <div className="card-footer">
                    <span>
                        <i className="fa fa-calendar" />
                        {` ${(this.props.card.dateCreated) ? format(this.props.card.dateCreated, "MM/DD/YYYY", { locale: ru }) : ""}`}
                    </span>
                    <span>
                        <i className="fa fa-user" />
                        {` ${(this.props.card.user) ? this.props.card.user.name : "Без имени"}`}
                    </span>

                </div>
            </div>

        );
    }

    public toggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
}
