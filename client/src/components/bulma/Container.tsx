import * as React from 'react';

export default class Container extends React.Component {
    public state = { showMenu: false }

    constructor(props: any) {
        super(props);
    }
    
    public render() {
        return (
            <div className="container">
                {this.props.children}
            </div>
        );
    }

    public toggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
}
