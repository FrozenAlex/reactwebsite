import * as React from 'react';
import Footer from 'src/components/layout/Footer';
// import Card from 'src/components/Card';
import headBack from './../../images/background.svg'
import Navbar from '../layout/Navbar';

const homeStyle: React.CSSProperties = {
    backgroundImage: `url(${headBack})`,
    backgroundPosition: 'bottom center',
    backgroundSize: "cover"
}


export default class HomePage extends React.Component {
    public state = {
        users: [],
    }
    constructor(props: any) {
        super(props);
    }
    public render() {
        return (
            <div>
                <Navbar fixed={true} />
                <div className="hero is-medium " style={homeStyle}>
                    <div className="hero-body has-text-centered has-text-white">
                        <h1 className="title has-text-white">Открытки</h1>
                        <h3 className="subtitle has-text-white">Сайт с открытками</h3>
                    </div>
                </div>
                <section className="section">
                    <div className="container" >
                        <h1 className="title">Наши открытки маленькие, занимают мало места и могут быть открыты на всех устройствах</h1>
                    </div>
                </section>
               <Footer/>
            </div>
        );
    }
}
