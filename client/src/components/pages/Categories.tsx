import * as React from 'react';
import { Card } from '../../models/Card';
// import CardTile from './CardTile';
import Axios from 'axios';
import { ChangeEvent } from 'react';
import { Category } from 'src/models/Category';
import Navbar from '../layout/Navbar';
import { Link } from 'react-router-dom';

interface IState {
    categories: Array<Partial<Category>>,
    search: string
    isLoading: boolean
}
export default class CategoriesPage extends React.Component<any, IState> {
    constructor(props: any) {
        super(props);

        // Начальное состояние
        this.state = {
            categories: [],
            search: '',
            isLoading: true
        }
    }

    public handleSearch = async (e: ChangeEvent<HTMLInputElement>) => {

        if (e.target.value !== this.state.search) {
            this.setState({
                search: e.target.value,
                isLoading: true
            })
        }
        const request = await Axios.get('/api/search', {
            params: {
                q: e.target.value
            }
        });

        const result: Array<Partial<Card>> = await request.data;

        result.map((card) => {
            if (typeof card.settings === 'string') {
                card.settings = JSON.parse(card.settings);
            }
            return card;
        })

        this.setState({
            categories: result,
            isLoading: false
        })
    }

    public async fetchData() {
        const request = await Axios.get('/api/category');

        const result: Array<Partial<Card>> = await request.data;

        result.map((card) => {
            if (typeof card.settings === 'string') {
                card.settings = JSON.parse(card.settings);
            }
            return card;
        })

        this.setState({
            categories: result,
            isLoading: false
        })
    }
    public componentDidMount() {
        this.fetchData();
    }

    public hadleClick(e: React.MouseEvent<HTMLDivElement>) {
        e.preventDefault();
    }
    public render() {
        return (
            <div>
                <Navbar fixed={true} />
                <div className="hero is-info">
                    <div className="hero-body has-text-centered">
                        <span className="title">Категории</span>
                        {/* <div className="form-group is-centered">
              <div className="control">
                <input className="input"
                  type="text"
                  placeholder="Find a repository"
                  onChange={this.handleSearch} />
              </div>
              <div className="control">
                <a className="button is-info">
                  Поиск
                </a>
              </div>
            </div> */}

                    </div>
                </div>
                <section className="section">
                    <div className="container" >
                        {
                            (this.state.isLoading) ?
                                (
                                    <h1 className="title has-text-centered">Загрузка списка...</h1>
                                ) : (
                                    <div className="categories" style={{ display: 'flex', justifyContent: 'space-between', flexWrap: "wrap" }}>
                                        {this.state.categories.map((cat: Partial<Category>) => (
                                            <Link to={`/category/${cat.id}`}>
                                                <div className="card">
                                                    {cat.id}
                                                    {cat.title}
                                                    {cat.description}
                                                </div>
                                            </Link>

                                        )
                                        )}
                                    </div>
                                )
                        }
                    </div>
                </section>
            </div>
        );
    }
}


// const category = () => {

// }