import * as React from 'react';
import Navbar from '../layout/Navbar';

export default class AboutPage extends React.Component {
  public render() {
    return (
      <div>
        <Navbar fixed={true} />
        <div className="hero is-warning is-medium">
          <div className="hero-body has-text-centered">
            <h1 className="title">О сайте</h1>
            <h3 className="subtitle">Информация о сайте и его создателях</h3>
          </div>
          
        </div>
        <div className="container">
          TODO
        </div>
      </div>
    );
  }
}
