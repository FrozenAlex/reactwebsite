import { Component, Fragment } from "react";
import React from 'react';
import Axios from 'axios';

interface ISettingsPageState {
    form: {
        password: string,
        newpassword: string,
        newpasswordrepeat: string
    },
    errors: {
        passwordsDontMatch: boolean
    }
}


class PasswordForm extends Component<any, ISettingsPageState>{
    constructor(props: ISettingsPageState) {
        super(props)

        this.state = {
            form: {
                password: '',
                newpassword: '',
                newpasswordrepeat: ''
            },
            errors: {
                passwordsDontMatch: false
            }
        }
    }
    render() {
        return (
            <Fragment>
                {(this.state) ? (
                    <form className="form-group" onSubmit={this.handleSubmit}>
                        <div className="field">
                            <label className="label">Старый пароль</label>
                            <div className="control has-icons-left has-icons-right">
                                <input className="input"
                                    type="password"
                                    name="password"
                                    autoComplete="current-password"
                                    value={this.state.form.password}
                                    onChange={this.handleChange} />
                                <span className="icon is-small is-left">
                                    <i className="fas fa-key" />
                                </span>
                                <span className="icon is-small is-right">
                                    {/* <i className="fas fa-exclamation-triangle" /> */}
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Новый пароль</label>
                            <div className="control has-icons-left has-icons-right">
                                <input className="input"
                                    type="password"
                                    name="newpassword" autoComplete="new-password"
                                    value={this.state.form.newpassword}
                                    onChange={this.handleChange} />
                                <span className="icon is-small is-left">
                                    <i className="fas fa-key" />
                                </span>
                                <span className="icon is-small is-right">
                                    {/* <i className="fas fa-exclamation-triangle" /> */}
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Повтор нового пароля</label>
                            <div className={`control has-icons-left has-icons-right ${(this.state.errors.passwordsDontMatch) ?('is-danger'): ('')}`}>
                                <input className="input"
                                    type="password"
                                    name="newpasswordrepeat" autoComplete="new-password"
                                    value={this.state.form.newpasswordrepeat}
                                    onChange={this.handleChange} />
                                <span className="icon is-small is-left">
                                    <i className="fas fa-key" />
                                </span>
                                <span className="icon is-small is-right">
                                    { (this.state.errors.passwordsDontMatch) ?(<i className="fas fa-exclamation-triangle" />): (null)}
                                </span>
                            </div>
                            { (this.state.errors.passwordsDontMatch) ?( <p className="help is-danger">Почта не валидна</p>): (null)}
                        </div>
                        <div className="field is-grouped">
                            <div className="control">
                                <button className="button is-link">Сохранить изменения</button>
                            </div>
                            <div className="control">
                                <button className="button is-text">Отмена</button>
                            </div>
                        </div>
                    </form>
                ) : (
                        <div>Загрузка...</div>
                    )}
            </Fragment>
        )
    }

    private handleChange = (event: any) => {
        this.setState({
            form: {
                ...this.state.form,
                [event.target.name]: event.target.value
            },
            errors: this.state.errors
        })
        this.validate();
    }

    private validate = () => {
        const passwordsDontMatch = this.state.form.newpassword !== this.state.form.newpasswordrepeat

        this.setState({errors: {
            passwordsDontMatch
        }})
    }

    private handleSubmit = async (event: any) => {
        event.preventDefault();
        const response = await Axios.post('/api/changepassword', this.state.form,
        {
            headers: {
                Authorization: localStorage.getItem('token')
            }
        })

        alert(JSON.stringify(response.data));

    }
}

export default PasswordForm;