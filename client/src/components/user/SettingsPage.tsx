import { Component, Fragment } from "react";
import React from 'react';
import Axios from 'axios';
import { User } from 'src/models/User';
import { Switch, Route } from 'react-router';
import UserForm from './UserForm';
import { NavLink } from 'react-router-dom';
import PasswordForm from './PasswordForm';

interface ISettingsPageState {
    user: Partial<User> | null
    password: string | null
}


class SettingsPage extends Component<any, ISettingsPageState>{
    constructor(props: ISettingsPageState) {
        super(props)

        this.state = {
            user: null,
            password: null
        }
    }

    public componentDidMount = async () => {
        const token = localStorage.getItem("token");
        const response = await Axios.get('/api/me', {
            headers: {
                Authorization: token
            }
        });
        // tslint:disable-next-line:no-console
        console.log(response.data);

        this.setState({
            user: response.data
        })
    }

    render() {
        return (
            <Fragment>
                <div className="hero is-info">
                    <div className="hero-body has-text-centered">
                        <h1 className="title">Настройки</h1>
                    </div>
                </div>
                <div className="container section">
                    <div className="columns">
                        <div className="column is-one-quarter">
                            <div className="userProile">
                                <div className="menu">
                                    <p className="menu-label">
                                        Настройки
                                    </p>
                                    <ul className="menu-list">
                                        <li>
                                            <NavLink exact={true} to="/settings" activeClassName="is-active" ><span><i className="fa fa-user-alt"/> Личные данные</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/settings/password" activeClassName="is-active" ><span><i className="fa fa-key"/> Смена пароля</span></NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <Switch>
                                <Route path='/settings/password' component={PasswordForm} />
                                <Route path='/settings' component={UserForm} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default SettingsPage;