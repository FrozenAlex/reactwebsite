import { Component, Fragment } from "react";
import React from 'react';
import Axios from 'axios';
import { User } from 'src/models/User';
import { connect } from 'react-redux';
import { setUser } from 'src/redux/actions/authActions';

interface ISettingsPageState {
    user: Partial<User> | null
    password: string | null
}


class UserForm extends Component<any, ISettingsPageState>{
    constructor(props: ISettingsPageState) {
        super(props)

        this.state = {
            user: null,
            password: null
        }
    }

    public componentDidMount = async () => {
        const token = localStorage.getItem("token");
        const response = await Axios.get('/api/me', {
            headers: {
                Authorization: token
            }
        });
        // tslint:disable-next-line:no-console
        console.log(response.data);

        this.setState({
            user: response.data
        })
    }

    render() {
        return (
            <Fragment>
                a
            </Fragment>
        )
    }

//     private handleChange = (event: any) => {

//         this.setState({
//             user: {
//                 ...this.state.user,
//                 [event.target.name]: event.target.value
//             }
//         })
//     }

//     private handleSubmit = async (event: any) => {
//         event.preventDefault();
//         if (this.state.user) {
//             const response = await Axios.put(`/api/user/${this.state.user.id}`, this.state.user, {
//                 headers: {
//                     Authorization: localStorage.getItem('token')
//                 }
//             });
//             alert(response.data)
//             if (response.status === 200){
//                 localStorage.setItem("user", JSON.stringify(this.state.user));
//                 this.props.setUser(this.state.user)
//             };
//         }

//     }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user.info
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setUser: (user: Partial<User>) => { dispatch(setUser(user)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);