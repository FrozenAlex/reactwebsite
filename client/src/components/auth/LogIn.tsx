import * as React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setUser } from 'src/redux/actions/authActions';
import { User } from 'src/models/User';
import Axios from 'axios';
import Navbar from '../layout/Navbar';
// import {connect} from 'react-redux';

interface ICredentials {
  username: string,
  password: string
}


class LoginPage extends React.Component<any, ICredentials> {

  constructor(props: any) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  public async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    try {
      const rawResponse = await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password
        })
      });
      const content = await rawResponse.json();
      const response = await Axios.get(`/api/user/${content.id}`)
      const user = response.data;

      this.props.setUser(user);
      
      // Пройти авторизацию
      localStorage.setItem("token", content.token);
      localStorage.setItem("user", JSON.stringify(user));
      
      this.props.history.push('/')
      
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.log(err); 
    }
  }

  public render() {
    return (
      <div>
        <Navbar fixed={true} />
        <div className="hero is-dark with-fixed-navbar">
          <div className="hero-body">
            <div className="columns is-centered">
              <div className="column is-centered" style={{ maxWidth: 300, margin: "0 auto 0 auto" }}>
                <h1 className="title has-text-centered">Вход</h1>
                <form onSubmit={this.handleSubmit}>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input className="input"
                      name="username"
                      type="text"
                      placeholder="Адрес почты"
                      value={this.state.username}
                      onChange={this.handleChange}
                      autoComplete="username" />
                      <span className="icon is-small is-left"><i className="fas fa-user" /></span>
                      
                    </p>
                  </div>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input className="input" autoComplete="current-password" name="password" type="password" placeholder="Пароль" value={this.state.password} onChange={this.handleChange} />
                      <span className="icon is-small is-left"><i className="fas fa-key" /></span>
                    </p>
                  </div>
                  <div className="field is-grouped is-grouped-right">
                    <p className="control">
                      <button type="submit" className="button is-success">
                        Войти
                    </button>
                    </p>
                  </div>
                </form>
                <p className="has-text-centered">Нет аккаунта?<br />
                  Зарегистрируйтесь <Link to="/register">здесь</Link></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    const value = event.target.value;

    // @ts-ignore
    this.setState({ [name]: value });
    this.validate();
  }

  private validate() {
    return true
  }
}

const mapStateToProps = (state:any)=>{
  return {
    user: state.user.info
  }
}

const mapDispatchToProps = (dispatch:any) => {
  return {
    setUser: (user: Partial<User>) => {dispatch(setUser(user))}
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginPage));