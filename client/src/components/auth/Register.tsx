import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Navbar from '../layout/Navbar';

interface IRegisterFormState {
  username: string,
  password: string
}
class RegisterPage extends React.Component<any, IRegisterFormState> {
  constructor(props: any) {
    super(props);
    this.state = { username: '', password: '' }

    // Binding to this *sigh*
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    const name = event.target.name;
    const value = event.target.value;

    // @ts-ignore
    this.setState({ [name]: value });
  }


  public async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    try {
      const rawResponse = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.state)
      });
      const content = await rawResponse.json();
      alert(`Регистрация успешна \n ${JSON.stringify(content)}`)
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.error(err)
    }
  }


  public validate() {
    return true
  }


  public render() {
    return (
      <div>
        <Navbar fixed={true} />
        <div className="hero is-dark with-fixed-navbar">
          <div className="hero-body">
            <div className="columns is-centered">
              <div className="column is-centered" style={{ maxWidth: 300, margin: "0 auto 0 auto" }}>
                <h1 className="title has-text-centered">Регистрация</h1>
                <form onSubmit={this.handleSubmit}>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input className="input" name="username" type="text" placeholder="Имя пользователя" value={this.state.username} onChange={this.handleChange} />
                      <span className="icon is-small is-left"><i className="fas fa-user" /></span>
                      {/* <span className="icon is-small is-right"><i className="fas fa-check" /></span> */}
                    </p>
                  </div>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input className="input" name="password" type="password" placeholder="Пароль" value={this.state.password} onChange={this.handleChange} />
                      <span className="icon is-small is-left"><i className="fas fa-key" /></span>
                      {/* <span className="icon is-small is-right"><i className="fas fa-check" /></span> */}
                    </p>
                  </div>
                  <div className="field is-grouped is-grouped-right">
                    <p className="control">
                      <button type="submit" className="button is-success">
                        Зарегистрироваться
                      </button>
                    </p>
                  </div>
                </form>
                <p className="has-text-centered">
                  Уже есть аккаунт?<br />
                  Попробуйте <Link to="/login">войти</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(RegisterPage)