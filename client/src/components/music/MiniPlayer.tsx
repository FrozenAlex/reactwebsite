import * as React from 'react';
import { Music } from 'src/models/Music';

interface IMiniPlayerProps {
    song: Partial<Music>,
    onDelete: any
}

enum PlayerState {
    loading, stopped, paused, playing, error
}

interface IMiniPlayerState {
    audio?: HTMLAudioElement | null, // Ссылка на главный элемент
    state: PlayerState
}

export default class MiniPlayer extends React.Component<IMiniPlayerProps, IMiniPlayerState> {

    constructor(props: IMiniPlayerProps) {
        super(props)

        this.play = this.play.bind(this)
        this.stop = this.stop.bind(this)
        this.onDelete = this.onDelete.bind(this)
        this.state = {
            state:PlayerState.loading
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    async componentDidMount() {
        const audio = new Audio(this.props.song.path);
        audio.preload = '1';
        audio.onended = () => {
            this.setState({ audio, state: PlayerState.paused});
        }
        if (audio) {
            this.setState({ audio, state: PlayerState.paused});
        }
    }

    async play() {
        if (this.state.audio) {
            this.state.audio.play();
            this.setState({state:PlayerState.playing})
        }
    }

    async stop() {
        if (this.state.audio) {
            this.state.audio.pause();
            this.setState({state:PlayerState.paused})
        }
    }

    // Остановить воспроизведение и удалить компонент
    componentWillUnmount(){
        if (this.state.audio) {
            this.state.audio.pause();
        }
    }

    async onDelete() {
        // Приостановить воспроизведение трека при удалении
        if(this.state.state === PlayerState.playing && this.state.audio) {
            this.state.audio.pause();
        }
        this.props.onDelete(this.props.song)
    }

    public render() {
        let playpausebutton;
        if (!this.state) {
            playpausebutton = <button className="button is-rounded is-small" disabled={true}><i className="fa fa-play"/></button>
        } else if (this.state.state === PlayerState.paused){
            playpausebutton = <button className="button is-rounded is-small is-primary" onClick={this.play}><i className="fa fa-play"/></button>
        } else {
            playpausebutton = <button className="button is-rounded is-small is-primary" onClick={this.stop}><i className="fa fa-stop"/></button>
        }
        return (
            <div className="song">
                <div className="song-info">
                    <span className="song-title">{this.props.song.title}</span>
                    <span className="song-artist">{this.props.song.artist}</span>
                </div>
                <div className="song-controls">
                    {playpausebutton}
                    <button className="button is-rounded is-small" onClick={this.onDelete}><i className="fa fa-trash"/></button>
                </div>
                
            </div>
        );
    }
}
