import React from 'react';
import Uploader from './Uploader';

export default class MusicUploader extends React.Component {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="hero is-warning is-small">
                    <div className="hero-body has-text-centered">
                        <h1 className="title">Загрузчик музыки</h1>
                        <h3 className="subtitle">Наполнение контентом</h3>
                    </div>
                </div>
                <Uploader />
            </div>
        )
    }
}