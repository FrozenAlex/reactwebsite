import * as React from 'react';
import { Music } from 'src/models/Music';
import Axios from 'axios';
import MiniPlayer from './MiniPlayer';
import Navbar from '../layout/Navbar';

interface IMusicState {
  songs: Array<Partial<Music>>
  current: number
}

export default class MusicPage extends React.Component<any, IMusicState> {

  constructor(props: any) {
    super(props)
    this.state = {
      songs: [],
      current: 1
    }

    this.deleteTrack = this.deleteTrack.bind(this);
  }

  async componentDidMount() {
    const response = await Axios.get('/api/music');
    const music = response.data;
    this.setState({ songs: music })
  }

  async deleteTrack(track: Partial<Music>) {
    // Удаление трека через API
    try {
      await Axios.delete('/api/music/' + track.id, {
        headers: {
          Authorization: localStorage.getItem('token')
        }
      });
      // Поиск трека в массиве песен
      const songIndex = this.state.songs.findIndex((song) => song.id === track.id);

      // Злостно копируем предыдущий массив и удаляем трек
      const songs = this.state.songs;
      songs.splice(songIndex, 1);

      // Обновляем состояние
      this.setState({ songs })
    } catch (err) {
      const response = err.response;
      if (response.data.error) {
        alert(response.data.error)
      }
    }
  }

  public render() {
    return (
      <div>
        <Navbar fixed={true} />
        <div className="hero is-warning">
          <div className="hero-body has-text-centered">
            <h1 className="title"><i className="fa fa-music" /></h1>
            <h3 className="subtitle">Музыка</h3>
          </div>
        </div>
        <div className="container">
          <div>
            {this.state.songs.map((song) => {
              return (<MiniPlayer key={song.id} song={song} onDelete={this.deleteTrack}>s</MiniPlayer>)
            })}
          </div>
        </div>
      </div>
    );
  }
}
