import React, { ChangeEvent } from 'react';
import axios from 'axios';

interface IUploaderState {
    selectedFiles?: FileList | null,
    loaded: number
}

export default class Uploader extends React.Component<any, IUploaderState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loaded: 0
        }

        this.handleFileSelection = this.handleFileSelection.bind(this)
        this.handleFileUpload = this.handleFileUpload.bind(this)
    }

    handleFileSelection(e: ChangeEvent<HTMLInputElement>) {
        e.preventDefault();
        this.setState({ selectedFiles: e.target.files })
    }

    async handleFileUpload() {
        if (this.state.selectedFiles && this.state.selectedFiles[0]) {
            const token = localStorage.getItem('token')
            const data = new FormData();

            data.append('file', this.state.selectedFiles[0]);

            const config = {
                headers: {
                    Authorization: token
                },
                onUploadProgress: (progressEvent: any) => {
                    this.setState({ loaded: Math.round((progressEvent.loaded * 100) / progressEvent.total) })
                }
            };
            await axios.post('/api/music', data, config)
            this.setState({
                selectedFiles: null
            })
        }

    }

    render() {
        let fileList = "";
        if (this.state.selectedFiles) {
            for (let index = 0; index < this.state.selectedFiles.length; index++) {
                const element = this.state.selectedFiles.item(index);
                if (element) {
                    fileList += `\n ${element.name}`
                }
            }
        }

        return (
            <div className="container">
                <div className="subtitle has-text-centered">Название трека берется из тегов ID3</div>
                <div className="file has-name is-boxed is-fullwidth">
                    <label className="file-label">
                        <input className="file-input" onChange={this.handleFileSelection} type="file" name="resume" />
                        <span className="file-cta">
                            <span className="file-icon">
                                <i className="fas fa-upload" />
                            </span>
                            <span className="file-label has-text-centered">
                                Выберите файл
                            </span>
                        </span>
                        <span className="file-name">
                            {fileList}
                        </span>
                    </label>
                </div>
                <div> Загружено {this.state.loaded}%</div>
                <button className="button is-primary" onClick={this.handleFileUpload}>Загрузить (Без прогресса)</button>
            </div>
        )
    }
}