import React, { FormEvent, ChangeEvent } from 'react';
import { Category } from 'src/models/Category';
import Axios from 'axios';

interface IAdminCategoryState {
    categories: Array<Partial<Category>>
    addForm: {
        title: string,
        description: string
    },
    edited: Partial<Category> | null
    dialog: "edit" | "confirm" | null
}

export default class AdminCategory extends React.Component<any, IAdminCategoryState> {
    constructor(props: any) {
        super(props);
        this.state = {
            categories: [],
            addForm: {
                title: "",
                description: ""
            },
            edited: null,
            dialog: null
        }
    }

    submitCategory = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const data = this.state.addForm;
        const token = localStorage.getItem('token')

        const request = await Axios.post("/api/category", data, {
            headers: {
                Authorization: token
            }
        })

        this.setState({
            addForm: {
                title: "",
                description: ""
            },
            categories: [
                ...this.state.categories,
                request.data
            ]
        })
    }

    formInput = (e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();


        this.setState({
            addForm: {
                ...this.state.addForm,
                [e.target.name]: e.target.value
            }
        })
    }

    async deleteCategory(id: number) {
        const token = localStorage.getItem('token')

        await Axios.delete(`/api/category/${id}`, {
            headers: {
                Authorization: token
            }
        });

        this.setState({
            categories: this.state.categories.filter(el => el.id !== id)
        })
    }

    componentDidMount = async () => {
        const response = await Axios.get('/api/category');

        const categories: Array<Partial<Category>> = response.data;

        this.setState({
            categories
        })
    }

    editItem(id: number) {
        const item = this.state.categories.find((el) => el.id === id);

        if (item) {
            this.setState({
                edited: item,
                dialog: "edit"
            })
        }
    }

    submitModal = async () => {
        // Нет предмета
        if (!this.state.edited) { return };

        const token = localStorage.getItem('token')

        if (this.state.edited) {
            await Axios.put(`/api/category/${this.state.edited.id}`, this.state.edited, {
                headers: {
                    Authorization: token
                }
            })
        }
        

        // @ts-ignore
        const index = this.state.categories.findIndex((item) => item.id === this.state.edited.id)
        const newCategories = this.state.categories;

        newCategories[index] = this.state.edited

        if (index >= 0) {
            this.setState({
                addForm: {
                    title: "",
                    description: ""
                },
                categories: newCategories,
                edited: null,
                dialog: null
            })
        }
    }

    modalFormInput = (e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();


        this.setState({
            edited: {
                ...this.state.edited,
                [e.target.name]: e.target.value
            }
        })
    }

    closeModal = () => {
        this.setState({
            edited: null,
            dialog: null
        })
    }

    render() {
        return (
            <div>
              
                <form className="field is-grouped" onSubmit={this.submitCategory}>
                    <p className="control is-expanded">
                        <input className="input" name="title" type="text" placeholder="Название" value={this.state.addForm.title} onChange={this.formInput} />
                    </p>
                    <p className="control is-expanded">
                        <input className="input" name="description" type="text" placeholder="Описание" value={this.state.addForm.description} onChange={this.formInput} />
                    </p>
                    <p className="control">
                        <button className="button is-info">
                            Добавить
                        </button>
                    </p>
                </form>
                <table className="table is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.categories.map((category) => (
                            <tr key={category.id}>
                                <th>{category.id}</th>
                                <th>{category.title}</th>
                                <th>{category.description}</th>
                                <th>
                                    <button className="button is-danger is-small" onClick={this.deleteCategory.bind(this, category.id)}>Удалить</button>
                                    <button className="button is-info is-small" onClick={this.editItem.bind(this, category.id)}>Изменить</button>
                                </th>
                            </tr>
                        ))}
                    </tbody>
                </table>
                {
                    ((this.state.dialog === "edit") && (this.state.edited)) ? (
                        <div className={`modal ${(this.state.dialog === "edit") ? "is-active" : ""}`}>
                            <div className="modal-background" onClick={this.closeModal} />
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">Редактирование категории</p>
                                    <button className="delete" aria-label="close" onClick={this.closeModal} />
                                </header>
                                <section className="modal-card-body">
                                    <form className="field is-grouped" onSubmit={this.submitCategory}>
                                        <p className="control is-expanded">
                                            <input className="input" name="title" type="text" placeholder="Название" value={this.state.edited.title} onChange={this.modalFormInput} />
                                        </p>
                                        <p className="control is-expanded">
                                            <input className="input" name="description" type="text" placeholder="Описание" value={this.state.edited.description} onChange={this.modalFormInput} />
                                        </p>
                                        <p className="control">
                                            <button className="button is-info">
                                                Добавить
                                            </button>
                                        </p>
                                    </form>
                                </section>
                                <footer className="modal-card-foot">
                                    <button className="button is-success" onClick={this.submitModal}>Сохранить изменения</button>
                                    <button className="button" onClick={this.closeModal}>Отмена</button>
                                </footer>
                            </div>
                        </div>
                    ) : ("")
                }
            </div>
        )
    }
}