import React from 'react';
import { User } from 'src/models/User';
import Axios from 'axios';

interface IAdminUsersState {
    users: Array<Partial<User>>
}

export default class AdminUsers extends React.Component<any, IAdminUsersState> {
    constructor(props: any) {
        super(props);
        this.state = {
            users: []
        }
    }

    async fetchData() {
        const token = localStorage.getItem('token')
        const response = await Axios.get('/api/user', {
            headers: {
                Authorization: token
            }
        });
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Ошибка статус ${response.status}`);
        }
    }

    componentDidMount = async () => {
        try {
            const data = await this.fetchData();
            this.setState({ users: data });
        } catch (err) {
            // @ts-ignore
            // tslint:disable-next-line:no-console
            console.log(err)
        }
    }

    toggleAdmin(id: number) {
        const index = this.state.users.findIndex((el) => el.id === id);

        const newArray = this.state.users;
        newArray[index].admin = !newArray[index].admin;

        this.setState({
            users: newArray
        })
    }


    render() {
        return (
            <div>
                Users table
                <table className="table is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ник</th>
                            <th>Имя</th>
                            <th>Почта</th>
                            <th>Открыток</th>
                            <th>Админ?</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.users.map((user) => (
                            <tr key={user.id}>
                                <th>{user.id}</th>
                                <th>{user.username}</th>
                                <th>{user.name}</th>
                                <th>{user.email}</th>
                                <th>{(user.cards) ? user.cards.length : '0'}</th>
                                <th onClick={this.toggleAdmin.bind(this, user.id)}>{(user.admin)?"Ага": "Неа"}</th>
                                <th>
                                    <button className="button is-danger is-small">Удалить</button>
                                    <button className="button is-info is-small">Изменить</button>
                                </th>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}


// const userTable = ({users: Array<Partial<User>>}) => {
//     return (
//         <div>
//             ass
//         </div>
//     )
// }