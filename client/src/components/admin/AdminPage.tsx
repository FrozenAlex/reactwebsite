import { Component, Fragment } from "react";
import React from 'react';
import Axios from 'axios';
import { User } from 'src/models/User';
import { Switch, Route } from 'react-router';

import { NavLink } from 'react-router-dom';
import AdminDashboard from './AdminDashboard';
import AdminMusic from './AdminMusic';
import AdminCard from './AdminCard';
import AdminCategory from './AdminCategory';
import AdminGenre from './AdminGenre';
import AdminUsers from './AdminUsers';
import Navbar from '../layout/Navbar';


interface ISettingsPageState {
    user: Partial<User> | null
    password: string | null
}


class AdminPage extends Component<any, ISettingsPageState>{
    constructor(props: ISettingsPageState) {
        super(props)

        this.state = {
            user: null,
            password: null
        }
    }

    public componentDidMount = async () => {
        const token = localStorage.getItem("token");
        const response = await Axios.get('/api/me', {
            headers: {
                Authorization: token
            }
        });
        // tslint:disable-next-line:no-console
        console.log(response.data);

        this.setState({
            user: response.data
        })
    }

    render() {
        return (
            <Fragment>
                <Navbar />
                <div className="hero is-info">
                    <div className="hero-body has-text-centered">
                        <h1 className="title">
                            Администрирование
                        </h1>
                    </div>
                </div>
                <div className="container section">
                    <div className="columns">
                        <div className="column is-one-quarter">
                            <div className="userProile">
                                <div className="menu">
                                    <p className="menu-label">
                                        Настройки
                                    </p>
                                    <ul className="menu-list">
                                        <li>
                                            <NavLink exact={true} to="/admin" activeClassName="is-active" ><span><i className="fa fa-tachometer-alt" /> Дашборд</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/admin/music" activeClassName="is-active" ><span><i className="fa fa-headphones-alt" /> Музыка</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/admin/cards" activeClassName="is-active" ><span><i className="fa fa-cat" /> Открытки</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/admin/category" activeClassName="is-active" ><span><i className="fa fa-tags" /> Категории</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/admin/genre" activeClassName="is-active" ><span><i className="fa fa-music" /> Жанры</span></NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/admin/users" activeClassName="is-active" ><span><i className="fa fa-user" /> Пользователи</span></NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <Switch>
                                <Route path='/admin/music' component={AdminMusic} />
                                <Route path='/admin/cards' component={AdminCard} />
                                <Route path='/admin/category' component={AdminCategory} />
                                <Route path='/admin/genre' component={AdminGenre} />
                                <Route path='/admin/users' component={AdminUsers} />
                                <Route path='/admin/' component={AdminDashboard} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default AdminPage;