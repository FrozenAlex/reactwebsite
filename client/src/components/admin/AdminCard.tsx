import React, { ChangeEvent } from 'react';
import Axios, { AxiosRequestConfig } from 'axios';


interface IUploaderState {
    selectedFiles?: FileList | null,
    loaded: number
}

export default class AdminCard extends React.Component<any, IUploaderState> {
    constructor(props: any) {
        super(props);
        this.state = {
            loaded: 0
        }
    }

    handleFileUpload = async () => {
        const token = localStorage.getItem('token')
        if (this.state.selectedFiles && this.state.selectedFiles[0]) {
            const data = new FormData();

            data.append('file', this.state.selectedFiles[0]);

            const config: AxiosRequestConfig = {
                headers: {
                    Authorization: token
                },
                onUploadProgress: (progressEvent: any) => {
                    this.setState({ loaded: Math.round((progressEvent.loaded * 100) / progressEvent.total) })
                }
            };
            
            try {
                const response = await Axios.post('/api/card', data, config)
                // Обработка ошибок 
                if (response.status !== 200) {
                    if (response.data.error) {
                        alert(response.data.error);
                    }
                } else {
                    this.setState({
                        selectedFiles: null
                    })
                }
            } catch(err) {
                const response = err.response;
                if(response.data.error){
                    alert(response.data.error)
                }
            }
        }
    }

    handleFileSelection = (e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.setState({ selectedFiles: e.target.files })
    }

    render() {
        let fileList = "";
        if (this.state.selectedFiles) {
            for (let index = 0; index < this.state.selectedFiles.length; index++) {
                const element = this.state.selectedFiles.item(index);
                if (element) {
                    fileList += `\n ${element.name}`
                }
            }
        }
        return (
            <div>
                <div className="container">
                    <div className="file has-name is-boxed">
                        <label className="file-label">
                            <input className="file-input" onChange={this.handleFileSelection} type="file" name="resume" />
                            <span className="file-cta">
                                <span className="file-icon">
                                    <i className="fas fa-upload" />
                                </span>
                                <span className="file-label has-text-centered">
                                    Выберите файл
                            </span>
                            </span>
                            <span className="file-name">
                                {fileList}
                            </span>
                        </label>
                    </div>
                    <div> Загружено {this.state.loaded}%</div>
                    <button className="button is-primary" onClick={this.handleFileUpload}>Загрузить</button>
                </div>

            </div>
        )
    }
}