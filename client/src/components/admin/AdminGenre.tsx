import React, { ChangeEvent, FormEvent } from 'react';
import Axios from 'axios';
import { Genre } from 'src/models/Genre';

interface IAdminGenreState {
    genres: Array<Partial<Genre>>,
    addForm: {
        title: string,
        description: string
    }
}

export default class AdminGenre extends React.Component<any, IAdminGenreState> {
    constructor(props: any) {
        super(props);
        this.state = {
            genres : [],
            addForm: {
                title: "",
                description: ""
            }
        }
    }

    async fetchData(){
        const response = await Axios.get('/api/genre');
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Ошибка статус ${response.status}`);
        }
    }

    formInput = (e: ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();


        this.setState({
            addForm: {
                ...this.state.addForm,
                [e.target.name]: e.target.value
            }
        })
    }

    submitGenre = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const data = this.state.addForm;
        const token = localStorage.getItem('token')

        const request = await Axios.post("/api/genre", data, {
            headers: {
                Authorization: token
            }
        })

        this.setState({
            addForm: {
                title: "",
                description: ""
            },
            genres: [
                ...this.state.genres,
                request.data
            ]
        })
    }

    async deleteGenre(id: number) {
        const token = localStorage.getItem('token')

        await Axios.delete(`/api/genre/${id}`, {
            headers: {
                Authorization: token
            }
        });

        this.setState({
            genres: this.state.genres.filter(el => el.id !== id)
        })
    } 


    componentDidMount = async () => {
        let data: Array<Partial<Genre>> // Массив жанров
        try {
            data = await this.fetchData();
            this.setState({genres:data});
        } catch(err) {
            // @ts-ignore
            // tslint:disable-next-line:no-console
            console.log(err)
        }      
    }

    render() {
        return (
            <div>
                Добавить жанров
                <form className="field is-grouped" onSubmit={this.submitGenre}>
                    <p className="control is-expanded">
                        <input className="input" name="title" type="text" placeholder="Название" value={this.state.addForm.title} onChange={this.formInput} />
                    </p>
                    <p className="control is-expanded">
                        <input className="input" name="description" type="text" placeholder="Описание" value={this.state.addForm.description} onChange={this.formInput} />
                    </p>
                    <p className="control">
                        <button className="button is-info">
                            Добавить
                        </button>
                    </p>
                </form>
                Жанры
                <table className="table is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Музыки</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.genres.map((genre) => (
                            <tr key={genre.id}>
                                <th>{genre.id}</th>
                                <th>{genre.title}</th>
                                <th>{genre.description}</th>
                                <th>{(genre.music) ? genre.music.length : '0'}</th>
                                <th>
                                    <button className="button is-danger is-small" onClick={this.deleteGenre.bind(this, genre.id)}>Удалить</button>
                                    <button className="button is-info is-small">Изменить</button>
                                </th>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}