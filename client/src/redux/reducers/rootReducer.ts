
import { User } from 'src/models/User';

const initState = {
    cards: [],
    user: {
        info: null,
        token: null
    }
}
const rootReducer = (state= initState, action: any) => {
    if (action.type === 'USER_SET'){
        const user: Partial<User> = action.user;
        const newState = {
            ...state,
            user:{
                ...state.user,
                info: user
            }
        }
        return newState
    }

    if (action.type === 'USER_LOGOUT'){
        const newState = {
            ...state,
            user: {
                info: null,
                token: null
            }
        }
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        return newState
    }
    return state;
}

export default rootReducer