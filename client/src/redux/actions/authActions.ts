import { User } from 'src/models/User';

// Вход
export const loginUser = (token: string) => {
    return {
        type: 'USER_AUTHENTICATE',
        token
    }
}

// Вход
export const setUser = (user: Partial<User>) => {
    return {
        type: 'USER_SET',
        user
    }
}

// Выход
export const logoutUser = () => {
    return {
        type: 'USER_LOGOUT'
    }
}