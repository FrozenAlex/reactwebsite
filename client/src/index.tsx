import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import rootReducer from './redux/reducers/rootReducer';
import { setUser } from './redux/actions/authActions';

const store = createStore(rootReducer, 
  // Для дебаггинга Redux
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const user = localStorage.getItem('user');
if (user) {
  store.dispatch(setUser(JSON.parse(user)))
}


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
