module.exports = {
    content: ['src/App.tsx', 'src/**/*.tsx', 'src/**/*.html'],
    css: ['src/css/App.css']
}