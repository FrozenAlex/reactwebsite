import * as Koa from 'koa';

// Koa modules
import * as passport from "koa-passport";
import * as session from "koa-session";
import * as bodyParser from "koa-bodyparser";
import * as koaStatic from "koa-static";
import * as views from "koa-views";
import * as moment from "moment";

// Set moment.js locale
moment.locale("ru");

// Koa app set up
export const app = new Koa();

// Serve static content
if (!process.env.NGINX) {
    app.use(koaStatic(__dirname + "/../../client/build/", {
        maxage: process.env.NODE_ENV === "production" ? 86400000 : 0
    }));
    app.use(koaStatic(__dirname + "/../public/", {
        maxage: process.env.NODE_ENV === "production" ? 86400000 : 0
    }));
}

// Рендерим HTML с pug
app.use(views(__dirname + '../views', {
    map: {
      pug: 'pug'
    }
}));

app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set("X-Response-Time", ms + "ms");
})

// Парсим тело запроса (ctx.request.body)
app.use(bodyParser());

app.keys = ["some secret lololo"];

const CONFIG = {
  key: "koa:sess",
  maxAge: 86400000,
  overwrite: true,
  httpOnly: true,
  signed: true,
  rolling: false,
  renew: false
};

app.use(session(CONFIG, app));

// Passport
app.use(passport.initialize());
app.use(passport.session());

require('./passport');

// Импорт 
import auth = require('./routes/auth')
import card = require('./routes/card')
import media = require('./routes/media')
import category = require('./routes/category')
import genre = require('./routes/genre')
import user = require('./routes/user')
import search = require('./routes/search')
import catchall = require('./routes/all')

// Подключение правил
app.use(auth.default.routes());
app.use(card.default.routes());
app.use(media.default.routes());
app.use(category.default.routes());
app.use(genre.default.routes());
app.use(user.default.routes());
app.use(search.default.routes());
app.use(catchall.default.routes());