import * as Router from "koa-router";

import { Card } from "../database/entity/Card";
import { Like } from "typeorm";

let router = new Router();

// Получить все жанры
router.get('/api/search', async (ctx, next) => {
    let cats = await Card.find({
        where:{
            title: Like(`%${ctx.query.q}%`)
        },
        order:{
            dateUpdated: 'DESC'
        }
    })

    ctx.body = cats;
})

export default router;