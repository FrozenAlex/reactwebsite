import * as Router from "koa-router";
import * as passport from "koa-passport";
import { User } from "../database/entity/User";

import jwt = require('jsonwebtoken');
import { getConnection } from "typeorm";

let router = new Router();

router.post('/api/login', async (ctx, next) => {
    await passport.authenticate('local', function (err, user) {
        if (user == false) {
            ctx.body = "Login failed";
        } else {
            //--payload - информация которую мы храним в токене и можем из него получать
            const payload = {
                id: user.id,
                username: user.username,
                name: user.name
            };
            const token = jwt.sign(payload, process.env.JWTSECRET); //здесь создается JWT

            ctx.body = { user: user.username, name: user.name, id: user.id, token: 'JWT ' + token };
        }
    })(ctx, next);
});


interface IChangePasswordForm {
    password: string
    newpassword: string
}
// TODO: Улучшить сброс пароля для админа
router.post('/api/changepassword',
    requireAuth,
    async (ctx, next) => {
        let form: IChangePasswordForm = ctx.request.body as IChangePasswordForm;

        if (ctx.state.user) {
            let user = await User.findOne(ctx.state.user.id, { select: ['password', 'username', 'id'] })
            if (await user.checkPassword(form.password)) {

                // Создаем новый пароль
                let newPassword = await user.hashPassword(form.newpassword)

                // Сохраняем его
                await getConnection()
                    .createQueryBuilder()
                    .update(User)
                    .set({password: newPassword})
                    .where("id = :id", {id :user.id})
                    .execute();
                
                // Шлем результат клиенту
                ctx.body = {
                    message: "Успешно"
                }

            } else {
                ctx.status = 400;
                ctx.body = {
                    error: 'Не правильный паролль'
                }
            }
        } else {
            ctx.body = {
                error: 'Ты зашел туда, где никто не должен быть. Что-то очень страшное творится на сервере'
            }
        }
        console.log(ctx.request.body)
    });

// TODO: Убрать эту функцию в /api/register 
router.post('/api/user',
    checkAuth,
    async (ctx, next) => {
        let data: any = ctx.request.body;
        if (data.username && data.password) {
            try {
                let userCount = await User.count();
                let user = new User();

                // Хэшим пароль.  почему бы и нет
                user.admin = (userCount === 0) // Если пользователей 0 то делаем тебя админом

                // Если админ создает пользователя, то даем создать админа
                if (ctx.state.user && ctx.state.user.admin) {
                    user.admin = data.admin;
                }

                user.name = data.name || "Без имени";
                user.password = await user.hashPassword(data.password);
                user.username = data.username;

                ctx.body = await user.save();
                console.log(user)
            }
            catch (err) {
                // Ловим ошибки
                ctx.status = 400;
                if (err.code === 'ER_DUP_ENTRY') {
                    return ctx.body = {
                        error: 'Пользователь с таким именем уже существует'
                    }
                }
                ctx.body = err;
            }
        } else {
            ctx.status = 400;
            ctx.body = {
                error: "Данных нет, нужно больше данных :)"
            };
        }
    });

router.get('/api/me', async (ctx, next) => {
    await passport.authenticate('jwt', async function (err, user) {
        if (user) {
            let baseUser = await User.findOne(user.id);
            ctx.body = baseUser;
        } else {
            ctx.status = 403
            ctx.body = {
                error: "Вы не вошли в аккаунт... Войдите пожалуйста!"
            };
        }
    })(ctx, next)
});


export default router;

/**
 * Требует авторизации и ложит пользователя в ctx.state.user
 * @param ctx Контекст
 * @param next Следующая функция
 */
export async function requireAuth(ctx: Router.IRouterContext, next: any) {
    await passport.authenticate('jwt', async function (err, user) {
        if (user) {
            ctx.state.user = user;
            console.log(user)
            await next()
        } else {
            ctx.status = 403
            ctx.body = { error: "Здесь требуется авторизация, сорри :(" };
            console.log("err", err)
        }
    })(ctx, next)
}

/**
 * Проверяет есть ли пользователь в токене. И ложит в ctx.state.user. Если нет то user = null
 * @param ctx Контекст
 * @param next Следующая функция
 */
export async function checkAuth(ctx: Router.IRouterContext, next: any) {
    await passport.authenticate('jwt', async function (err, user) {
        ctx.state.user = user;
        await next()
    })(ctx, next)
}