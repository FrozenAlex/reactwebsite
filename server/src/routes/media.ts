import * as Router from "koa-router";

import * as multer from "koa-multer";
import * as ID3 from "node-id3";

import { Music } from "../database/entity/Music";
import { checkAuth, requireAuth } from "./auth";
import { userInfo } from "os";
import { Like } from "typeorm";

let destination = __dirname + '/../../public/uploads/';

// Set up storage
var storage = multer.diskStorage({
    destination: destination,
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const upload = multer({
    storage: storage
});

let router = new Router();


// Поиск музыки
router.get("/api/music", async (ctx, next) => {
    // GET параметры
    let music: Music[]
    
    // Если в URL присутствует GET параметр ?search=
    if (ctx.query.search) {
        music = await Music.find({where: {
            title: Like(`%${ctx.query.search}%`)
        }})
    } else {
        music = await Music.find({relations: ["genre"]})
    }

    // Получить всю музыку
    ctx.body = music
});

/**
 * Открыть один трек
 */
router.get("/api/music/:id", async (ctx, next) => {
    // GET параметры
    let params: any = ctx.params;

    // Получить трек с id
    ctx.body = await Music.findOne(params.id, { relations: ["genre"] });
});

/**
 * Публикация трека
 */
router.post("/api/music",
    checkAuth,
    async (ctx, next) => {
        // Загружать может лишь админ
        if (ctx.state.user.admin) {
            await next();
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администратор может загружать музыку, сорри"
            }
        }
    },
    upload.single("file"), // Получим файл
    async (ctx) => {
        // Запрос с файлом
        let request = <multer.MulterIncomingMessage>ctx.req;

        // Читаем тэги из файла
        let tags = await ID3.read(request.file.path);

        let music = new Music();
        music.path = "/uploads/" + request.file.filename
        music.artist = request.body.artist || tags.artist;
        music.title = request.body.title || tags.title;

        // Сохраняю запись в базе данных
        ctx.body = await music.save();
    }
);

/**
 * Удаление трека
 */
router.del("/api/music/:id",
    requireAuth,
    async (ctx, next) => {
        let params: any = ctx.params;

        // Поиск музыки с ID
        let music = await Music.findOne(params.id);
        if (ctx.state.user.admin) {
            // Удаление музыки
            if (music) {
                ctx.body = await music.remove();
            } else {
                ctx.status = 404;
                ctx.body = {
                    error: "Такого трека нет. Попробуйте удалить другой :D"
                }
            }
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администратор может удалять музыку"
            }
        }

    }
);


export default router;