import * as Router from "koa-router";

import { Genre } from "../database/entity/Genre";
import { requireAuth } from "./auth";

let router = new Router();

// Получить все жанры
router.get('/api/genre', async (ctx, next) => {
    try {
        let genres = await Genre.find()
        ctx.body = genres;
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            error: "Ошибка в соединении с базой данных"
        }
    }
})

// Получить жанр
router.get('/api/genre/:id', async (ctx) => {
    try {
        let genre = await Genre.findOne(ctx.params.id, { relations: ["music"] })
        ctx.body = genre;
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            error: "Ошибка в соединении с базой данных"
        }
    }

})

// Создать жанр
router.post('/api/genre',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let body: Genre = ctx.request.body as Genre;

            let genre = new Genre();
            genre.description = body.description;
            genre.title = body.title;
            ctx.body = await genre.save();

        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут создавать жанры"
            }
        }
    }
)

// Изменить жанр
router.put('/api/genre/:id',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let body: Genre = ctx.request.body as Genre;

            let genre = await Genre.findOne(ctx.params.id)
            genre.description = genre.description || body.description;
            genre.title = genre.title || body.title;
            ctx.body = await genre.save();
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут изменять жанры"
            }
        }
    }
)

// Удалить жанр
router.del('/api/genre/:id',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let genre = await Genre.findOne(ctx.params.id)
            if (genre) {
                ctx.body = await Genre.delete(genre);
            } else {
                ctx.status = 404;
                ctx.body = {
                    error: "Нет такого жанра"
                }
            }
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут удалять жанры"
            }
        }
    }
)



export default router;