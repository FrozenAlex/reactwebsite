import * as Router from "koa-router";

import { Category } from "../database/entity/Category";
import { requireAuth } from "./auth";
import { userInfo } from "os";

let router = new Router();

// Получить все жанры
router.get('/api/category', async (ctx) => {
    let cats = await Category.find()
    ctx.body = cats;
})


// Получить категорию
router.get('/api/category/:id', async (ctx) => {
    let categories = await Category.findOne(ctx.params.id, { relations: ["cards"] })
    ctx.body = categories;
})

// Создать категорию
router.post('/api/category',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let body: Category = ctx.request.body as Category;

            let category = new Category();
            category.description = body.description;
            category.title = body.title;
            ctx.body = await category.save();

        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут создавать категории"
            }
        }
    }
)

// Изменить категорию
router.put('/api/category/:id',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let body: Category = ctx.request.body as Category;

            let category = await Category.findOne(ctx.params.id)
            category.description =  body.description || category.description ;
            category.title =body.title || category.title;
            ctx.body = await category.save();
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут изменять категории"
            }
        }
    }
)

// Удалить категорию
router.del('/api/category/:id',
    requireAuth,
    async (ctx) => {
        if (ctx.state.user.admin) {
            let category = await Category.findOne(ctx.params.id)
            if (category) {
                ctx.body = await Category.delete(category);
            } else {
                ctx.status = 404;
                ctx.body = {
                    error: "Нет такой категории"
                }
            }
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администраторы могут изменять категории"
            }
        }
    }
)


export default router;