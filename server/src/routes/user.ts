import * as Router from "koa-router";

import { User } from "../database/entity/User";
import { checkAuth, requireAuth } from "./auth";

let router = new Router();

// Получить всех пользователей
router.get('/api/user',
    checkAuth,
    async (ctx, next) => {
        let users
        // Если есть в базе и админ то берем все данные, иначе краткий список
        if (ctx.state.user && ctx.state.user.admin) {
            users = await User.find({ select: ["id", "name", "username", "admin", "email"], relations: ["cards"] })
        } else {
            users = await User.find({ select: ["id", "name"], relations: ["cards"] })
        }
        ctx.body = users;
    }
)

// Получить одного пользователя
router.get('/api/user/:id', async (ctx, next) => {
    let user = await User.findOne(ctx.params.id, {
        relations: ["cards"]
    })
    ctx.body = user;
})

// Обновить данные одного пользователя
router.put('/api/user/:id',
    requireAuth, // Получаем пользователя в ctx.state.user или ничего
    async (ctx, next) => {
        let params = ctx.params;
        let userdata: Partial<User> = ctx.request.body;

        let user = await User.findOne(params.id);

        if (user) {
            // Если это тот же самый пользователь или администратор
            if ((user.id === ctx.state.user.id) || ctx.state.user.admin) {
                user.name = userdata.name || user.name;
                user.username = userdata.username || user.username;
                user.email = userdata.email || user.username;

                // Разрешаем админу наделять других своим статусом 'admin'
                if (ctx.state.user.admin) {
                    user.admin = userdata.admin || user.admin
                }

                // Тут мы берем все данные пользователя, так что хватит
                ctx.body = await user.save();
            } else {
                ctx.status = 403
                ctx.body = {
                    error: 'Редактировать пользователей могут лишь сами пользователи и администратор'
                }
            }

        } else {
            ctx.status = 404
            ctx.body = {
                error: 'Не найден такой пользователь'
            }
        }

    })

// Удалить пользователя
router.del('/api/user/:id',
    requireAuth,
    async (ctx, next) => {
        let params = ctx.params;

        if (params.id) {
            let user = await User.findOne(params.id);
            if ((user.id === ctx.state.user.id) || ctx.state.user.admin) {
                ctx.body = await User.delete(user);
            } else {
                ctx.status = 403;
                ctx.body = {
                    error: "Удалять пользователей могут лишь администраторы и сами пользователи"
                }
            }
        } else {
            ctx.status = 404;
            ctx.body = {
                error: "C запросом что-то не так. ID параметра нет OwO !? Если ты не админ то напиши мне :^)"
            }
        }

    })

export default router;