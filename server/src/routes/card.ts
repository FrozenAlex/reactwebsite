import * as Router from "koa-router";
import { User } from "../database/entity/User";

import { Card, ICardMusicSetting } from "../database/entity/Card";
import * as multer from "koa-multer";
import * as extract from 'extract-zip';
import * as path from 'path';
import * as fs from 'fs-extra';
import * as sanitizeHtml from 'sanitize-html';

import { checkAuth, requireAuth } from "./auth";


let destination = __dirname + '/../../public/card/';

// Set up storage
var storage = multer.diskStorage({
    destination: destination,
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const upload = multer({
    storage: storage
});


let router = new Router();

// Получить одну карточку
router.get('/api/card/:url', async (ctx, next) => {
    let getParameters: any = ctx.params;
    let card = await Card.findOne({
        where: {
            url: getParameters.url
        }
    })
    ctx.body = card;
})

// Получить все карточки
router.get('/api/card', async (ctx, next) => {
    let cards = await Card.find({
        where: {
            public: true,
        }, relations: ['user']
    })
    ctx.body = cards;
})

// Получить одну карточку
router.get('/api/card/:id', async (ctx, next) => {
    let params = ctx.params;
    try {
        // Открытка с ID
        const card = await Card.findOne({ url: params.id }, { relations: ["category", "user"] });

        // Проверка на существование и отправка
        if (card) {
            ctx.body = card;
        } else {
            ctx.status = 404;
            ctx.body = { message: "Не найдено" }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
})

// Сделать еще одну копию открытки
router.put('/api/card/:id/fork',
    requireAuth, // Требовать входа пользователей
    async (ctx, next) => {
        let params = ctx.params;
        try {
            // Открытка с ID
            const card = await Card.findOne(params.id, { relations: ["category", "user"] });
            const user = await User.findOne(ctx.state.user.id);

            // Создание новой карточки и перенос значений
            let newCard = new Card();
            newCard.title = card.title;
            newCard.description = card.description;
            newCard.category = card.category;
            newCard.parent = card;
            newCard.settings = card.settings;
            newCard.public = card.public;
            newCard.user = user;
            newCard.url = Card.makeid(8);

            // Сохранение в базе данных
            newCard = await newCard.save();
            console.log(newCard);
            // Копирование файлов открытки
            let oldCardPath = path.resolve(destination, card.url);
            let newCardPath = path.resolve(destination, newCard.url);
            await fs.copy(oldCardPath, newCardPath)

            // Запись нового Manifest файла
            await fs.writeJSON(path.resolve(newCardPath, 'manifest.json'), newCard);

            ctx.body = newCard;
        } catch (err) {
            console.log(err);
            ctx.status = 500;
            ctx.body = err;
        }
    })

// Обновить карточку
router.put('/api/card/:id',
    requireAuth,
    async (ctx, next) => {
        let params: Card = ctx.params;
        let newCard: Partial<Card> = ctx.request.body;

        let card = await Card.findOne(params.id, { relations: ['user'] });

        console.log(card)
        if (card) {
            // Если пользователь создавший карточку или админ (всемогущий)
            if ((card.user.id === ctx.state.user.id) || ctx.state.user.admin) {
                let cardID = card.url;
                let cardPath = path.resolve(destination, cardID);

                // Проверить все вещи на XSS
                if (newCard.settings) {
                    for (let setting of newCard.settings) {
                        // Убирание html из пожеланий (стоп хакерам)
                        if (setting.type === "text") {
                            setting.value = sanitizeHtml(setting.value, {
                                allowedTags: [],
                                allowedAttributes: {}
                            });
                        }

                        // Трансформация музычки (копирование в папочку)
                        if (setting.type === "music") {
                            if (setting.path === "") {
                                // Старый параметр
                                let old = card.settings.find((it) => (it.id === setting.id)) as ICardMusicSetting

                                let src = __dirname + '/../../public';

                                // Формируем пути
                                let source = path.join(src, setting.track.path);
                                let dest = path.join(destination, card.url, old.path)

                                // Копируем
                                await fs.copy(source, dest)
                                setting.path = old.path;

                                console.log(setting)
                            }
                        }
                    }
                }

                // Основные опции
                card.title = sanitizeHtml(newCard.title, {
                    allowedTags: [],
                    allowedAttributes: {}
                });
                card.description = sanitizeHtml(newCard.description, {
                    allowedTags: [],
                    allowedAttributes: {}
                });

                // Установка трека
                card.settings = newCard.settings;

                // Записать изменения на диск и в бд
                await fs.writeJSON(path.resolve(cardPath, 'manifest.json'), card);
                await card.save();
                ctx.body = { message: 'Success' }
            } else {
                ctx.body = { message: 'Нет доступа' }
            }
        } else {
            ctx.body = { message: 'Такой открытки нет' }
        }
    })

// Удалить карточку
router.del('/api/card/:id',
    requireAuth,
    async (ctx, next) => {
        let params: Card = ctx.params;

        let card = await Card.findOne(params.id, { relations: ['user'] });

        if (card) {
            // Если пользователь создавший карточку или админ (всемогущий)
            if ((card.user.id === ctx.state.user.id) || ctx.state.user.admin) {
                let cardID = card.url;
                let cardPath = path.resolve(destination, cardID);
                await fs.remove(cardPath);
                await Card.remove(card);
                ctx.body = { message: 'Успешно' }
            } else {
                ctx.status = 403;
                ctx.body = { error: 'Открытки удалять может только пользователь-создатель или администратор' }
            }
        } else {
            ctx.status = 404;
            ctx.body = { message: 'Такой открытки нет' }
        }
    }
);

/**
 * Публикация открытки через архив
 */
router.post("/api/card",
    requireAuth,
    async (ctx, next) => {
        // Загружать может лишь админ
        if (ctx.state.user.admin) {
            await next();
        } else {
            ctx.status = 403;
            ctx.body = {
                error: "Только администратор может загружать открытки, сорри"
            }
        }
    },
    upload.single("file"), // Получим файл
    async (ctx, next) => {
        // Запрос с файлом
        let request = <multer.MulterIncomingMessage>ctx.req;
        let cardID = Card.makeid(5);
        let user = await User.findOne(ctx.state.user.id);

        let cardPath = path.resolve(destination, cardID);

        // Распаковка открытки
        await unzip(request.file.path, cardPath);
        // Удаление архива с открыткой
        fs.unlink(request.file.path);

        let manifest: Partial<Card> = await fs.readJSON(path.resolve(cardPath, 'manifest.json'))
        console.log(manifest);

        if (manifest) {
            let card = new Card();
            card.title = manifest.title;
            card.description = manifest.description;
            card.url = cardID;
            card.public = manifest.public || true;
            card.parent = null;
            card.settings = manifest.settings;
            card.user = user;
            ctx.body = await card.save();
            console.log(card)
        }
    }
);


export default router;

function unzip(file, destination) {
    return new Promise((resolve, reject) => {
        extract(file, { dir: destination }, (err) => (err) ? reject(err) : resolve())
    })
}