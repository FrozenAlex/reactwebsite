import * as Router from "koa-router";
import * as fs from 'fs-extra'
let router = new Router();

// Великий редирект на приложение React
router.get('*', async (ctx,next) => {
    ctx.body = await fs.readFile(__dirname + '/../../../client/build/index.html', { encoding: 'utf8'});
})

export default router;