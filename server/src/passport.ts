import { getConnection } from "typeorm";
import { User } from "./database/entity/User";

import * as passport from 'koa-passport';
const jwtsecret = "mysecretkey";

import * as LocalStrategy from 'passport-local';
import * as JwtStrategy from 'passport-jwt';


const jwtOptions = {
    jwtFromRequest: JwtStrategy.ExtractJwt.fromAuthHeader,
    secretOrKey: process.env.JWTSECRET
  };

passport.serializeUser(async function (user: User, done: Function) {
    try {
        let connection = await getConnection();
        console.log(user);

        await connection.manager.save(user)

        return done(null, user.id)
    } catch (err) {
        return done(err);
    }
})

passport.deserializeUser(async function (id, done) {
    try {
        let connection = await getConnection();
        let user = await connection.manager.findOne(User, id);

        if (user) {
            return done(null, user);
        } else {
            return done(null, null);
        }
    } catch (err) {
        return done(err)
    }
})


passport.use(new LocalStrategy.Strategy({
    session:true,
    passwordField: 'password',
    usernameField: 'username'
}, async function (username: string, password: string, done) {
    try {
        let user = await User.findOne({username:username}, {select: ["password", "username", "id", "admin"]})
        console.log(user)
        if (user) {
            let correct = await user.checkPassword(password);
            if (correct) {
                return done(null, user)
            } else return done(null, false, {message: "Неверный пароль"})
        }

        else done(null, false, {message: "Нет такого пользователя"});
    } catch (err) {
        done(err)
    }
}))


passport.use(new JwtStrategy.Strategy({
    secretOrKey: process.env.JWTSECRET,
    jwtFromRequest:  JwtStrategy.ExtractJwt.fromAuthHeaderWithScheme("jwt"),
}, async function (payload, done) {
    try {
        // Получим нашего пользователя
        let user = await User.findOne(payload.id)
       
        if (user) {
          done(null, user)
        } else {
          done(null, false)
        }
    } catch(err) {
        return done(err)
    }
  })
);