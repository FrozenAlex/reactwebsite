import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne } from "typeorm";
import { Genre } from "./Genre";

/**
 * Музыкальная композиция
 */
@Entity()
export class Music extends BaseEntity{
    // ID трека
    @PrimaryGeneratedColumn()
    id: number;

    // Название трека
    @Column()
    title: string;

    // Исполнитель
    @Column()
    artist: string;

    // Путь к треку
    @Column()
    path:string;

    // Жанр трека (Только для индексации)
    @ManyToOne(type => Genre, genre => genre.music)
    genre: Genre;
}
