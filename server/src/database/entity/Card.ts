import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, BaseEntity, ManyToOne, OneToMany, UpdateDateColumn, CreateDateColumn } from "typeorm";
import { User } from "./User";
import { Category } from "./Category";
import { Music } from "./Music";
import { MaxLength, IsString } from "class-validator";

/**
 * Открытка
 */
@Entity()
export class Card extends BaseEntity {
    // Id открытки
    @PrimaryGeneratedColumn()
    id: number;

    // Заголовок открытки
    @MaxLength(30, {
        message: "Сообщение слишком длинное"
    })
    @Column()
    title: string;

    // Описание открытки
    @Column()
    description?: string;

    // Короткая ссылка на открытку
    @IsString()
    @Column({ type: "varchar", length: 8 })
    url: string;

    // Публичная ли карточка
    @Column()
    public: boolean;

    // Данные о открытке в формате JSON
    @Column({ type: 'json', name:'settings'})
    settings: Array<ICardTextSetting | ICardJSSetting | ICardCSSSetting | ICardMusicSetting>

    // Категория открытки
    @ManyToOne(type => Category, category => category.cards)
    category: Category;

    // Пользователь - создатель открытки
    @ManyToOne(type => User, user => user.cards)
    user?: User;

    @ManyToOne(() => Card, card => card.children,  {onDelete:"SET NULL"})
    parent?: Card;

    @OneToMany(() => Card, card => card.parent, {onDelete:"SET NULL"})
    children?: Card[];

    // Дата создания
    @CreateDateColumn()
    dateCreated?: Date;

    // Дата обновления (Не должно случаться, ну а вдруг)
    @UpdateDateColumn()
    dateUpdated?: Date;

    @BeforeInsert()
    updateInsertDates() {
        if (this.url === null) this.url = Card.makeid(8);
    }

    public static makeid(length: number): string {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}

interface ICardCSSSetting {
    id: string,
    name: string,
    type: 'css',
    selector: string,
    property: string,
    value: string
}
interface ICardJSSetting {
    id: string,
    name: string,
    type: 'js',
    editor: 'color' | 'text' | 'slider',
    value: string
}

interface ICardTextSetting {
    id: string,
    name: string,
    type: 'text',
    selector: string,
    value: string
}

export interface ICardMusicSetting {
    id: string,
    name: string,
    type: 'music',
    path: string, // Путь к музыке
    track: Partial<Music> // Трек их базы данных (Для копирования)
}