import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from "typeorm";
import { Music } from "./Music";

/**
 * Жанр музыки
 */
@Entity()
export class Genre extends BaseEntity {
    // ID жанра
    @PrimaryGeneratedColumn()
    id: number;

    // Название жанра
    @Column()
    title: string;

    // Описание жанра
    @Column()
    description?: string;

    // Музыка в этом жанре
    @OneToMany(type => Music, music => music.genre)
    music: Music[]
}
