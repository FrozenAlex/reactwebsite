import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, BaseEntity, OneToMany } from "typeorm";
import { Card } from "./Card";

/**
 * Категория открыток
 */
@Entity()
export class Category extends BaseEntity {
    // ID категории
    @PrimaryGeneratedColumn()
    id: number;
    
    // Название категории
    @Column()
    title: string;

    // Описание категории
    @Column()
    description?: string;

    // Открытки, в этой категории
    @OneToMany(type => Card, card => card.category)
    cards: Card[];
}
