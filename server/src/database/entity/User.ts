import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from "typeorm";
import { validate, Length, IsEmail, IsBoolean } from "class-validator";

import { compare, hash, genSalt } from "bcryptjs";
import { Card } from "./Card";

/**
 * Пользователь системы
 */
@Entity()
export class User extends BaseEntity {
    // ID 
    @PrimaryGeneratedColumn()
    id: number;

    // Отображаемое имя пользователя
    @Length(5, 40, {
        message: "Имя должно быть не менее 5 символов и не более 40!"
    })
    @Column({ default: 'Anonimous' })
    name: string;

    // Почта
    @IsEmail()
    @Column({ nullable: true })
    email?: string;

    // Имя пользователя (логин)
    @Column({ unique: true, length: 25 })
    username: string;

    // Хэшированый пароль пользователя
    @Column({ select: false })
    password: string;

    // Открытки, созданные пользователем
    @OneToMany(type => Card, card => card.user)
    cards: Card[];

    // Администратор или нет
    @IsBoolean()
    @Column({ default: false })
    admin: boolean

    // Проверить пароль на верность с помощью bcrypt
    checkPassword(password: string) {
        return compare(password, this.password);
    }

    // Создать хэш пароля с помощью bcrypt
    hashPassword(password: string): Promise<string> {
        return genSalt(10).then(salt => hash(password, salt));
    }

    // Сменить пароль (пока не используется)
    async changePassword(password) {
        this.password = await this.hashPassword(password);
    }
}
