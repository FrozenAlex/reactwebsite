require('dotenv').config({ path: __dirname + '/../.env' })
import "reflect-metadata";
import {createConnection} from "typeorm";
import {app} from "./server";

createConnection().then(async connection => {
    app.listen(process.env.PORT);
    
}).catch(error => console.log(error));
