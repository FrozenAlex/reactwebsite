require('dotenv').config({ path: __dirname + '/.env' })


module.exports = {
   "type": "mysql",
   "host": process.env.DB_HOST,
   "port": process.env.DB_PORT,
   "username": process.env.DB_USER,
   "password": process.env.DB_PASSWORD,
   "database": process.env.DB_NAME,
   "synchronize": false,
   "logging": true,
   "entities": [
      "build/database/entity/**/*.js"
   ],
   "migrations": [
      "build/database/migration/**/*.js"
   ],
   "subscribers": [
      "build/database/subscriber/**/*.js"
   ],
   "cli": {
      "entitiesDir": "src/database/entity",
      "migrationsDir": "src/database/migration",
      "subscribersDir": "src/database/subscriber"
   }
}